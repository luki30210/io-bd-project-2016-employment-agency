package com.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Doswiadczenie
 *
 */
@Entity

public class Doswiadczenie implements Serializable {

	private static final long serialVersionUID = 1L;

	public Doswiadczenie() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_doswiadczenia;

	private String stanowisko;

	private String branza;

	@Temporal(TemporalType.DATE)
	private Date data_rozp_pracy;

	@Temporal(TemporalType.DATE)
	private Date data_zak_pracy;

	private String zdobyte_umiejetnosci;

	@ManyToOne
	@JoinColumn(name = "ID_klienta")
	private Klient_poszukujacy_pracy klient_poszukujacy;

	public Integer getId_doswiadczenia() {
		return id_doswiadczenia;
	}

	public void setId_doswiadczenia(Integer id_doswiadczenia) {
		this.id_doswiadczenia = id_doswiadczenia;
	}

	public String getStanowisko() {
		return stanowisko;
	}

	public void setStanowisko(String stanowisko) {
		this.stanowisko = stanowisko;
	}

	public String getBranza() {
		return branza;
	}

	public void setBranza(String branza) {
		this.branza = branza;
	}

	public Date getData_rozp_pracy() {
		return data_rozp_pracy;
	}

	public void setData_rozp_pracy(Date data_rozp_pracy) {
		this.data_rozp_pracy = data_rozp_pracy;
	}

	public Date getData_zak_pracy() {
		return data_zak_pracy;
	}

	public void setData_zak_pracy(Date data_zak_pracy) {
		this.data_zak_pracy = data_zak_pracy;
	}

	public String getZdobyte_umiejetnosci() {
		return zdobyte_umiejetnosci;
	}

	public void setZdobyte_umiejetnosci(String zdobyte_umiejetnosci) {
		this.zdobyte_umiejetnosci = zdobyte_umiejetnosci;
	}

	public Klient_poszukujacy_pracy getKlient_poszukujacy() {
		return klient_poszukujacy;
	}

	public void setKlient_poszukujacy(Klient_poszukujacy_pracy klient_poszukujacy) {
		this.klient_poszukujacy = klient_poszukujacy;
	}

}
