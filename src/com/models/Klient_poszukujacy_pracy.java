package com.models;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Klient_poszukujacy_pracy
 *
 */
@Entity

public class Klient_poszukujacy_pracy implements Serializable {

	private static final long serialVersionUID = 1L;

	public Klient_poszukujacy_pracy() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idKlienta;

	private String login;

	private String haslo;

	private String imie;

	private String nazwisko;

	@Temporal(TemporalType.DATE)
	private Date dataUrodzenia;

	@Enumerated(EnumType.STRING)
	private plec plec;

	private String email;

	private String nrTel;

	@Temporal(TemporalType.DATE)
	private Date dataRejestracji;

	private Blob zdjecie;

	private String infoDodatkowe;

	private String kategoriaPrawaJazdy;

	private String jezykiObce;

	@OneToMany(mappedBy = "klient_poszukujacy")
	private List<Wyksztalcenie> wyksztalcenie;

	@OneToMany(mappedBy = "klient_poszukujacy")
	private List<Doswiadczenie> doswiadczenie;
	
	@ManyToMany(cascade=CascadeType.REMOVE)
	@JoinTable(name = "Aplikacja_klienta", joinColumns = @JoinColumn(name = "ID_klienta"),inverseJoinColumns = @JoinColumn(name = "ID_ogloszenia"))
	private List<Ogloszenie> ogloszenie;

	
	public Integer getIdKlienta() {
		return idKlienta;
	}

	public void setIdKlienta(Integer idKlienta) {
		this.idKlienta = idKlienta;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	/*
	public boolean porownajHaslo() throws NoSuchAlgorithmException, InvalidKeySpecException {
		return PBKDF2.validatePassword(haslo, this.haslo);
	}

	public void setHaslo(String haslo) throws NoSuchAlgorithmException, InvalidKeySpecException {
		this.haslo = PBKDF2.generateStorngPasswordHash(haslo);
	}
	*/	

	public String getImie() {
		return imie;
	}

	public String getHaslo() {
		return haslo;
	}

	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public Date getDataUrodzenia() {
		return dataUrodzenia;
	}

	public void setDataUrodzenia(Date dataUrodzenia) {
		this.dataUrodzenia = dataUrodzenia;
	}

	public plec getPlec() {
		return plec;
	}

	public void setPlec(plec plec) {
		this.plec = plec;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNrTel() {
		return nrTel;
	}

	public void setNrTel(String nrTel) {
		this.nrTel = nrTel;
	}

	public Blob getZdjecie() {
		return zdjecie;
	}

	public void setZdjecie(Blob zdjecie) {
		this.zdjecie = zdjecie;
	}

	public String getInfoDodatkowe() {
		return infoDodatkowe;
	}

	public void setInfoDodatkowe(String infoDodatkowe) {
		this.infoDodatkowe = infoDodatkowe;
	}

	public String getKategoriaPrawaJazdy() {
		return kategoriaPrawaJazdy;
	}

	public void setKategoriaPrawaJazdy(String kategoriaPrawaJazdy) {
		this.kategoriaPrawaJazdy = kategoriaPrawaJazdy;
	}

	public String getJezykiObce() {
		return jezykiObce;
	}

	public void setJezykiObce(String jezykiObce) {
		this.jezykiObce = jezykiObce;
	}

	public Date getDataRejestracji() {
		return dataRejestracji;
	}

	public void setDataRejestracji(Date dataRejestracji) {
		this.dataRejestracji = dataRejestracji;
	}

	public List<Wyksztalcenie> getWyksztalcenie() {
		return wyksztalcenie;
	}

	public void setWyksztalcenie(List<Wyksztalcenie> wyksztalcenie) {
		this.wyksztalcenie = wyksztalcenie;
	}

	public List<Doswiadczenie> getDoswiadczenie() {
		return doswiadczenie;
	}

	public void setDoswiadczenie(List<Doswiadczenie> doswiadczenie) {
		this.doswiadczenie = doswiadczenie;
	}

	public List<Ogloszenie> getOgloszenie() {
		return ogloszenie;
	}

	public void setOgloszenie(List<Ogloszenie> ogloszenie) {
		this.ogloszenie = ogloszenie;
	}

	
}
