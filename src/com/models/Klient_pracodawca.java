package com.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Klient_pracodawca
 *
 */
@Entity

public class Klient_pracodawca implements Serializable {

	private static final long serialVersionUID = 1L;

	public Klient_pracodawca() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_pracodawcy;

	private String login;

	private String haslo;

	@Temporal(TemporalType.DATE)
	private Date dataRejestracji;

	private String email;

	private String nrTel;
	
	//dane opcjonalne - podawane przy edycji
	//****
	private String nazwa_firmy;
	
	private String adres;
	
	private String kraj;
	
	private String region;
	//****
	
	@OneToMany(mappedBy = "klient")
	private List<Ogloszenie> ogloszenie;

	
	public String getNazwa_firmy() {
		return nazwa_firmy;
	}

	public void setNazwa_firmy(String nazwa_firmy) {
		this.nazwa_firmy = nazwa_firmy;
	}

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}

	public String getKraj() {
		return kraj;
	}

	public void setKraj(String kraj) {
		this.kraj = kraj;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Integer getId_pracodawcy() {
		return id_pracodawcy;
	}

	public void setId_pracodawcy(Integer id_pracodawcy) {
		this.id_pracodawcy = id_pracodawcy;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getHaslo() {
		return haslo;
	}

	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNrTel() {
		return nrTel;
	}

	public void setNrTel(String nrTel) {
		this.nrTel = nrTel;
	}

	public Date getDataRejestracji() {
		return dataRejestracji;
	}

	public void setDataRejestracji(Date dataRejestracji) {
		this.dataRejestracji = dataRejestracji;
	}

	public List<Ogloszenie> getOgloszenie() {
		return ogloszenie;
	}

	public void setOgloszenie(List<Ogloszenie> ogloszenie) {
		this.ogloszenie = ogloszenie;
	}
	
	

}
