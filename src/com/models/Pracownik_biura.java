package com.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Pracownik_biura
 *
 */
@Entity

public class Pracownik_biura implements Serializable {

	private static final long serialVersionUID = 1L;

	public Pracownik_biura() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_pracownika;
	
	private String login;
	
	private String haslo;
	
	private String imie;
	
	private String nazwisko;
	
	@Enumerated(EnumType.STRING)
	private rodzaj_konta typ_konta;
	
	private String email;
	
	@OneToMany(mappedBy = "pracownik")
	private List<Ogloszenie> ogloszenie;

	public Integer getId_pracownika() {
		return id_pracownika;
	}

	public void setId_pracownika(Integer id_pracownika) {
		this.id_pracownika = id_pracownika;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getHaslo() {
		return haslo;
	}

	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public rodzaj_konta getTyp_konta() {
		return typ_konta;
	}

	public void setTyp_konta(rodzaj_konta typ_konta) {
		this.typ_konta = typ_konta;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Ogloszenie> getOgloszenie() {
		return ogloszenie;
	}

	public void setOgloszenie(List<Ogloszenie> ogloszenie) {
		this.ogloszenie = ogloszenie;
	}

}
