package com.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Ogloszenie
 *
 */
@Entity

public class Ogloszenie implements Serializable {

	private static final long serialVersionUID = 1L;

	public Ogloszenie() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_ogloszenia;

	private String branza;

	private String miasto;

	private String przedzialWiekowy;

	@Enumerated(EnumType.STRING)
	private poziom_szkoly poziomWyksztalcenia;

	private String doswiadczenie;

	private String opisStanowiska;

	private String nazwaFirmy;

	private String adres;

	private String kraj;

	private String region;

	@Temporal(TemporalType.DATE)
	private Date terminWaznosciOgl;

	@ManyToOne
	@JoinColumn(name = "ID_pracodawcy")
	private Klient_pracodawca klient;

	@ManyToOne
	@JoinColumn(name = "ID_pracownika")
	private Pracownik_biura pracownik;

	@ManyToMany(mappedBy = "ogloszenie")
	private List<Klient_poszukujacy_pracy> klient_poszukujacy;

	public Integer getId_ogloszenia() {
		return id_ogloszenia;
	}

	public void setId_ogloszenia(Integer id_ogloszenia) {
		this.id_ogloszenia = id_ogloszenia;
	}

	public String getBranza() {
		return branza;
	}

	public void setBranza(String branza) {
		this.branza = branza;
	}

	public String getMiasto() {
		return miasto;
	}

	public void setMiasto(String miasto) {
		this.miasto = miasto;
	}

	public String getPrzedzialWiekowy() {
		return przedzialWiekowy;
	}

	public void setPrzedzialWiekowy(String przedzialWiekowy) {
		this.przedzialWiekowy = przedzialWiekowy;
	}

	public poziom_szkoly getPoziomWyksztalcenia() {
		return poziomWyksztalcenia;
	}

	public void setPoziomWyksztalcenia(poziom_szkoly poziomWyksztalcenia) {
		this.poziomWyksztalcenia = poziomWyksztalcenia;
	}

	public String getDoswiadczenie() {
		return doswiadczenie;
	}

	public void setDoswiadczenie(String doswiadczenie) {
		this.doswiadczenie = doswiadczenie;
	}

	public String getOpisStanowiska() {
		return opisStanowiska;
	}

	public void setOpisStanowiska(String opisStanowiska) {
		this.opisStanowiska = opisStanowiska;
	}

	public String getNazwaFirmy() {
		return nazwaFirmy;
	}

	public void setNazwaFirmy(String nazwaFirmy) {
		this.nazwaFirmy = nazwaFirmy;
	}

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}

	public String getKraj() {
		return kraj;
	}

	public void setKraj(String kraj) {
		this.kraj = kraj;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Date getTerminWaznosciOgl() {
		return terminWaznosciOgl;
	}

	public void setTerminWaznosciOgl(Date terminWaznosciOgl) {
		this.terminWaznosciOgl = terminWaznosciOgl;
	}

	public Pracownik_biura getPracownik() {
		return pracownik;
	}

	public void setPracownik(Pracownik_biura pracownik) {
		this.pracownik = pracownik;
	}

	public Klient_pracodawca getKlient() {
		return klient;
	}

	public void setKlient(Klient_pracodawca klient) {
		this.klient = klient;
	}

	public List<Klient_poszukujacy_pracy> getKlient_poszukujacy() {
		return klient_poszukujacy;
	}

	public void setKlient_poszukujacy(List<Klient_poszukujacy_pracy> klient_poszukujacy) {
		this.klient_poszukujacy = klient_poszukujacy;
	}

}
