package com.models;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Odwiedziny
 *
 */
@Entity

public class Odwiedziny implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Odwiedziny() {
		super();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_odwiedzin;
	
	private Integer dzien;
	
	private Integer miesiac;
	
	private Integer rok;
	
	private Integer liczba_odwiedzin;

	public Integer getId_odwiedzin() {
		return id_odwiedzin;
	}

	public void setId_odwiedzin(Integer id_odwiedzin) {
		this.id_odwiedzin = id_odwiedzin;
	}

	public Integer getDzien() {
		return dzien;
	}

	public void setDzien(Integer dzien) {
		this.dzien = dzien;
	}

	public Integer getMiesiac() {
		return miesiac;
	}

	public void setMiesiac(Integer miesiac) {
		this.miesiac = miesiac;
	}

	public Integer getRok() {
		return rok;
	}

	public void setRok(Integer rok) {
		this.rok = rok;
	}

	public Integer getLiczba_odwiedzin() {
		return liczba_odwiedzin;
	}

	public void setLiczba_odwiedzin(Integer liczba_odwidzin) {
		this.liczba_odwiedzin = liczba_odwidzin;
	}

}
