package com.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Wyksztalcenie
 *
 */
@Entity

public class Wyksztalcenie implements Serializable {

	private static final long serialVersionUID = 1L;

	public Wyksztalcenie() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_wyksztalcenia;
	
	@Enumerated(EnumType.STRING)
	private poziom_szkoly poziom;
	
	private String szkola;
	
	private String kierunek;
	
	private String specjalizacja;
	
	@Temporal(TemporalType.DATE)
	private Date data_rozp_nauki;
	
	@Temporal(TemporalType.DATE)
	private Date data_zak_nauki;
	
	private Boolean stan;
	
	@ManyToOne
	@JoinColumn(name = "ID_klienta")
	private Klient_poszukujacy_pracy klient_poszukujacy;

	public Integer getId_wyksztalcenia() {
		return id_wyksztalcenia;
	}

	public void setId_wyksztalcenia(Integer id_wyksztalcenia) {
		this.id_wyksztalcenia = id_wyksztalcenia;
	}

	public poziom_szkoly getPoziom() {
		return poziom;
	}

	public void setPoziom(poziom_szkoly poziom) {
		this.poziom = poziom;
	}

	public String getSzkola() {
		return szkola;
	}

	public void setSzkola(String szkola) {
		this.szkola = szkola;
	}

	public String getKierunek() {
		return kierunek;
	}

	public void setKierunek(String kierunek) {
		this.kierunek = kierunek;
	}

	public String getSpecjalizacja() {
		return specjalizacja;
	}

	public void setSpecjalizacja(String specjalizacja) {
		this.specjalizacja = specjalizacja;
	}

	public Date getData_rozp_nauki() {
		return data_rozp_nauki;
	}

	public void setData_rozp_nauki(Date data_rozp_nauki) {
		this.data_rozp_nauki = data_rozp_nauki;
	}

	public Date getData_zak_nauki() {
		return data_zak_nauki;
	}

	public void setData_zak_nauki(Date data_zak_nauki) {
		this.data_zak_nauki = data_zak_nauki;
	}

	public Klient_poszukujacy_pracy getKlient_poszukujacy() {
		return klient_poszukujacy;
	}

	public void setKlient_poszukujacy(Klient_poszukujacy_pracy klient_poszukujacy) {
		this.klient_poszukujacy = klient_poszukujacy;
	}

	public Boolean getStan() {
		return stan;
	}

	public void setStan(Boolean stan) {
		this.stan = stan;
	}
	
}
