package com.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.models.Klient_poszukujacy_pracy;
import com.models.Klient_pracodawca;
import com.models.Pracownik_biura;
import com.models.rodzaj_konta;

/**
 * Session Bean implementation class LoginService
 */
@Stateless
@LocalBean
public class LoginService {

	// stworzenie polaczenia z baza oraz obiektu posredniczacego
	@PersistenceContext(unitName = "BiuroPosrednictwaPracyBCC")
	EntityManager em;

	/**
	 * Default constructor.
	 */
	public LoginService() {
		// TODO Auto-generated constructor stub
	}

	public Integer porownajLoginPoszukujacego(String login, String haslo) {
		TypedQuery<Klient_poszukujacy_pracy> zapytanie1 = em.createQuery(
				"SELECT a FROM Klient_poszukujacy_pracy a WHERE a.login=:login", Klient_poszukujacy_pracy.class);
		zapytanie1.setParameter("login", login);

		List<Klient_poszukujacy_pracy> lista1 = zapytanie1.getResultList();

		if (lista1.size() == 1) {
			try {
				if (PBKDF2.validatePassword(haslo, lista1.get(0).getHaslo())) {
					return lista1.get(0).getIdKlienta();
				}
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	public Integer porownajLoginPracodawcy(String login, String haslo) {
		TypedQuery<Klient_pracodawca> zapytanie2 = em
				.createQuery("SELECT b FROM Klient_pracodawca b WHERE b.login=:login", Klient_pracodawca.class);
		zapytanie2.setParameter("login", login);

		List<Klient_pracodawca> lista1 = zapytanie2.getResultList();

		if (lista1.size() == 1) {
			try {
				if (PBKDF2.validatePassword(haslo, lista1.get(0).getHaslo())) {
					return lista1.get(0).getId_pracodawcy();
				}
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	public Integer porownajLoginPracownika(String login, String haslo) {
		TypedQuery<Pracownik_biura> zapytanie3 = em.createQuery("SELECT c FROM Pracownik_biura c WHERE c.login=:login",
				Pracownik_biura.class);
		zapytanie3.setParameter("login", login);

		List<Pracownik_biura> lista1 = zapytanie3.getResultList();

		if (lista1.size() == 1) {
			try {
				if (PBKDF2.validatePassword(haslo, lista1.get(0).getHaslo())) {
					return lista1.get(0).getId_pracownika();
				}
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	
	public rodzaj_konta getRodzajKontaPracownikaBiura (Integer idPracownikaBiura) {
	    TypedQuery<Pracownik_biura> query = em.createQuery("SELECT c FROM Pracownik_biura c WHERE c.id_pracownika=:id_pracownika", Pracownik_biura.class);
	    query.setParameter("id_pracownika", idPracownikaBiura);
	    return query.getSingleResult().getTyp_konta();
	  }
}
