package com.service;

import java.util.Calendar;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.models.Odwiedziny;

/**
 * Session Bean implementation class OdwiedzinyService
 */
@Stateless
@LocalBean
public class OdwiedzinyService {

	@PersistenceContext(unitName = "BiuroPosrednictwaPracyBCC")
	EntityManager em;

	/**
	 * Default constructor.
	 */
	public OdwiedzinyService() {
		// TODO Auto-generated constructor stub
	}

	public void dodaj() {
		Calendar calendar = Calendar.getInstance();
		TypedQuery<Odwiedziny> zapytanie = em.createQuery(
				"SELECT o from Odwiedziny o WHERE o.dzien=:dzien and o.miesiac=:miesiac and o.rok=:rok",
				Odwiedziny.class);
		zapytanie.setParameter("dzien", calendar.get(Calendar.DAY_OF_MONTH));
		zapytanie.setParameter("miesiac", calendar.get(Calendar.MONTH) + 1);
		zapytanie.setParameter("rok", calendar.get(Calendar.YEAR));
		List<Odwiedziny> list = zapytanie.getResultList();

		if (list.size() == 0) {
			Odwiedziny odw = new Odwiedziny();
			odw.setDzien(calendar.get(Calendar.DAY_OF_MONTH));
			odw.setMiesiac(calendar.get(Calendar.MONTH) + 1);
			odw.setRok(calendar.get(Calendar.YEAR));
			odw.setLiczba_odwiedzin(1);
			em.persist(odw);
		} else {
			Query q = em.createQuery(
					"Update Odwiedziny SET liczba_odwiedzin=:liczba_odwiedzin WHERE id_odwiedzin=:id");
			q.setParameter("id", list.get(0).getId_odwiedzin());
			q.setParameter("liczba_odwiedzin", list.get(0).getLiczba_odwiedzin() + 1);
			q.executeUpdate();
		}
	}


	
	public Integer liczbaOdwiedzinDzien(Integer dzien, Integer miesiac, Integer rok) {
	    TypedQuery<Odwiedziny> zapytanie = em.createQuery(
	        "SELECT o FROM Odwiedziny o WHERE o.dzien=:dzien AND o.miesiac=:miesiac AND o.rok=:rok",
	        Odwiedziny.class);
	    zapytanie.setParameter("dzien", dzien);
	    zapytanie.setParameter("miesiac", miesiac);
	    zapytanie.setParameter("rok", rok);
	    return zapytanie.getSingleResult().getLiczba_odwiedzin();
	  }
	
	
	public Integer liczbaOdwiedzinMiesiac(Integer miesiac, Integer rok) {
	    Query zapytanie = em.createQuery(
	          "SELECT SUM(o.liczba_odwiedzin) FROM Odwiedziny o WHERE o.miesiac=:miesiac AND o.rok=:rok");
	      zapytanie.setParameter("miesiac", miesiac);
	      zapytanie.setParameter("rok", rok);
	      return ((Number)zapytanie.getSingleResult()).intValue();
	  }
	
	
	public Integer liczbaOdwiedzinRok(Integer rok) {
	    Query zapytanie = em.createQuery(
	        "SELECT SUM(o.liczba_odwiedzin) FROM Odwiedziny o WHERE  o.rok=:rok");
	    zapytanie.setParameter("rok", rok);
	    return ((Number)zapytanie.getSingleResult()).intValue();
	  }
}
