package com.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.models.Klient_poszukujacy_pracy;
import com.models.Wyksztalcenie;

/**
 * Session Bean implementation class WyksztalcenieService
 */
@Stateless
@LocalBean
public class WyksztalcenieService {

	/**
	 * Default constructor.
	 */
	public WyksztalcenieService() {
		// TODO Auto-generated constructor stub
	}

	@PersistenceContext(unitName = "BiuroPosrednictwaPracyBCC")
	EntityManager em;

	public void dodajWyksztalcenie(Wyksztalcenie w) {
		em.persist(w);
	}

	public void dodajWyksztalcenieDoPoszukujacego(Integer idWyksz, Integer idPosz) {
		TypedQuery<Klient_poszukujacy_pracy> pQuery = em.createQuery(
				"SELECT p FROM Klient_poszukujacy_pracy p WHERE p.idKlienta=:idKlienta",
				Klient_poszukujacy_pracy.class);
		pQuery.setParameter("idKlienta", idPosz);

		Klient_poszukujacy_pracy k = pQuery.getSingleResult();

		TypedQuery<Wyksztalcenie> wQuery = em.createQuery(
				"SELECT w FROM Wyksztalcenie w WHERE w.id_wyksztalcenia=:id_wyksztalcenia", Wyksztalcenie.class);
		wQuery.setParameter("id_wyksztalcenia", idWyksz);
		
		Wyksztalcenie w = wQuery.getSingleResult();
		
		List<Wyksztalcenie> wList = k.getWyksztalcenie();
		wList.add(w);
		k.setWyksztalcenie(wList);
		w.setKlient_poszukujacy(k);
		
	}
	
	public List<Wyksztalcenie> wyswietlWyksztalcenie(Integer id)
	{
		TypedQuery<Klient_poszukujacy_pracy> pQuery = em.createQuery("SELECT p from Klient_poszukujacy_pracy p"
				+ " where p.idKlienta = :id",Klient_poszukujacy_pracy.class);
		pQuery.setParameter("id", id);
		Klient_poszukujacy_pracy k = pQuery.getSingleResult();
		return k.getWyksztalcenie();
		//reszta w servlecie podobnie jak w wyswietlOdloszenieService / jsp 
	}
	
	public void usunWyksztalcenie(Integer idWyksztalcenia) {
	    Query q = em.createQuery("DELETE FROM Wyksztalcenie w WHERE d.id_wyksztalcenia=:id_wyksztalcenia");
	    q.setParameter("id_wyksztalcenia", idWyksztalcenia);
	    q.executeUpdate();
	  }

}
