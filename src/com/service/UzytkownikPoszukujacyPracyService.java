package com.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.models.Doswiadczenie;
import com.models.Klient_poszukujacy_pracy;
import com.models.Ogloszenie;
import com.models.Wyksztalcenie;

/**
 * Session Bean implementation class UzytkownikPoszukujacyPracyService
 */
@Stateless
@LocalBean
public class UzytkownikPoszukujacyPracyService {

	@PersistenceContext(unitName = "BiuroPosrednictwaPracyBCC")
	EntityManager em;
	
	@EJB
	  DoswiadczenieService ds;
	
	@EJB
	  WyksztalcenieService ws;


	/**
	 * Default constructor.
	 */
	public UzytkownikPoszukujacyPracyService() {
		// TODO Auto-generated constructor stub
	}

	public void dodajUzytkownika(Klient_poszukujacy_pracy klient) {
		em.persist(klient);
	}

	public boolean czyLoginZajety(String login) {
		TypedQuery<Klient_poszukujacy_pracy> zapytanie = em.createQuery(
				"SELECT k from Klient_poszukujacy_pracy k WHERE k.login=:login", Klient_poszukujacy_pracy.class);
		zapytanie.setParameter("login", login);

		List<Klient_poszukujacy_pracy> lista = zapytanie.getResultList();

		if (lista.size() == 1)
			return true;
		else
			return false;
	}

	public Klient_poszukujacy_pracy pobierzDanePoszukujacego(Integer id) {
		TypedQuery<Klient_poszukujacy_pracy> zapytanie = em.createQuery(
				"SELECT k from Klient_poszukujacy_pracy k WHERE k.idKlienta=:id", Klient_poszukujacy_pracy.class);
		zapytanie.setParameter("id", id);

		Klient_poszukujacy_pracy k = zapytanie.getSingleResult();

		return k;
	}

	public void edytujDanePoszukujacego(Klient_poszukujacy_pracy k) {
		Query q = em.createQuery(
				"UPDATE Klient_poszukujacy_pracy SET haslo=:haslo, email=:email, nazwisko=:nazwisko, jezykiObce=:jezyki, infoDodatkowe=:inf_dod, nrTel=:nrTel, kategoriaPrawaJazdy=:kategoria where idKlienta=:id");
		q.setParameter("id", k.getIdKlienta());
		q.setParameter("haslo", k.getHaslo());
		q.setParameter("email", k.getEmail());
		q.setParameter("nazwisko", k.getNazwisko());
		q.setParameter("nrTel", k.getNrTel());
		q.setParameter("jezyki", k.getJezykiObce());
		q.setParameter("inf_dod", k.getInfoDodatkowe());
		q.setParameter("kategoria", k.getKategoriaPrawaJazdy());
		q.executeUpdate();

	}

	public void usunKontoPoszukujacego(Integer idPoszukujacegoPracy) {
	    List<Doswiadczenie> doswiadczenia = pobierzDanePoszukujacego(idPoszukujacegoPracy).getDoswiadczenie();
	    for (Doswiadczenie doswiadczenie : doswiadczenia) {
	      ds.usunDoswiadczenie(doswiadczenie.getId_doswiadczenia());
	    }
	    
	    List<Wyksztalcenie> wyksztalcenia = pobierzDanePoszukujacego(idPoszukujacegoPracy).getWyksztalcenie();
	    for(Wyksztalcenie wyksztalcenie : wyksztalcenia) {
	      ws.usunWyksztalcenie(wyksztalcenie.getId_wyksztalcenia());
	    }
	    
	    Query q = em.createQuery("DELETE FROM Klient_poszukujacy_pracy k WHERE k.idKlienta=:idKlienta");
	    q.setParameter("idKlienta", idPoszukujacegoPracy);
	    q.executeUpdate();
	  }
	
	public void aplikacja(Integer idOgl, Integer idPosz){
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Klient_poszukujacy_pracy> cqKlient = builder.createQuery(Klient_poszukujacy_pracy.class);
		Root<Klient_poszukujacy_pracy> kRoot = cqKlient.from(Klient_poszukujacy_pracy.class);
		cqKlient.select(kRoot).where(builder.equal(kRoot.get("idKlienta").as(Integer.class), idPosz));
		TypedQuery<Klient_poszukujacy_pracy> kQuery = em.createQuery(cqKlient);
		Klient_poszukujacy_pracy k = kQuery.getSingleResult();
		
		builder = em.getCriteriaBuilder();
		CriteriaQuery<Ogloszenie> cqOgloszenie = builder.createQuery(Ogloszenie.class);
		Root<Ogloszenie> oRoot = cqKlient.from(Ogloszenie.class);
		cqOgloszenie.select(oRoot).where(builder.equal(oRoot.get("id_ogloszenia").as(Integer.class), idOgl));
		TypedQuery<Ogloszenie> oQuery = em.createQuery(cqOgloszenie);
		Ogloszenie o = oQuery.getSingleResult();
		
		List<Klient_poszukujacy_pracy> kList = o.getKlient_poszukujacy();
		kList.add(k);
		o.setKlient_poszukujacy(kList);
		k.getOgloszenie().add(o);
	}
	
	public Integer getLiczbaUzytkownikowPoszukujacych() {
	    return ((Number)em.createQuery("SELECT COUNT(k.idKlienta) FROM Klient_poszukujacy_pracy k ").getSingleResult()).intValue();
	    }
}
