package com.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.models.Doswiadczenie;
import com.models.Klient_poszukujacy_pracy;

/**
 * Session Bean implementation class DoswiadczenieService
 */
@Stateless
@LocalBean
public class DoswiadczenieService {

	/**
	 * Default constructor.
	 */
	public DoswiadczenieService() {
		// TODO Auto-generated constructor stub
	}

	@PersistenceContext(unitName = "BiuroPosrednictwaPracyBCC")
	EntityManager em;

	public void dodajDoswiadczenie(Doswiadczenie d) {
		em.persist(d);
	}

	public void dodajDoswiadczenieDoPoszukujacego(Integer idDosw, Integer idPosz) {
		TypedQuery<Klient_poszukujacy_pracy> pQuery = em.createQuery(
				"SELECT p FROM Klient_poszukujacy_pracy p WHERE p.idKlienta=:idKlienta",
				Klient_poszukujacy_pracy.class);
		pQuery.setParameter("idKlienta", idPosz);

		Klient_poszukujacy_pracy k = pQuery.getSingleResult();

		TypedQuery<Doswiadczenie> dQuery = em.createQuery(
				"SELECT d FROM Doswiadczenie d WHERE d.id_doswiadczenia=:id_doswiadczenia", Doswiadczenie.class);

		dQuery.setParameter("id_doswiadczenia", idDosw);
		Doswiadczenie d = dQuery.getSingleResult();

		List<Doswiadczenie> dList = k.getDoswiadczenie();
		dList.add(d);
		k.setDoswiadczenie(dList);
		d.setKlient_poszukujacy(k);
	}

	public List<Doswiadczenie> wyswietlDoswiadczenie(Integer id) {
		TypedQuery<Klient_poszukujacy_pracy> pQuery = em.createQuery(
				"SELECT k FROM Klient_poszukujacy_pracy k where k.idKlienta=:id", Klient_poszukujacy_pracy.class);
		pQuery.setParameter("id", id);
		Klient_poszukujacy_pracy k = pQuery.getSingleResult();

		return k.getDoswiadczenie();
	}
	
	public void usunDoswiadczenie(Integer idDoswiadczenia) {
	    Query q = em.createQuery("DELETE FROM Doswiadczenie d WHERE d.id_doswiadczenia=:id_doswiadczenia");
	    q.setParameter("id_doswiadczenia", idDoswiadczenia);
	    q.executeUpdate();
	  }
}
