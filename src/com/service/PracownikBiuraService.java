package com.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.models.Ogloszenie;
import com.models.Pracownik_biura;

/**
 * Session Bean implementation class PracownikBiuraService
 */
@Stateless
@LocalBean
public class PracownikBiuraService {

	@PersistenceContext(unitName = "BiuroPosrednictwaPracyBCC")
	EntityManager em;

	/**
	 * Default constructor.
	 */
	public PracownikBiuraService() {
	}

	public void dodajPracBiura(Pracownik_biura pracownik) {
		TypedQuery<Pracownik_biura> zapytanie = em.createQuery(
				"SELECT p from Pracownik_biura p WHERE p.login=:login", Pracownik_biura.class);
		zapytanie.setParameter("login", pracownik.getLogin());

		List<Pracownik_biura> lista = zapytanie.getResultList();

		if (lista.size() == 0)
			em.persist(pracownik);
	}
	
	public void zaakceptujOgloszenie(Integer idOgl, Integer idPrac){
		TypedQuery<Pracownik_biura> pQuery = em.createQuery(
				"SELECT p FROM Pracownik_biura p WHERE p.id_pracownika=:id",
				Pracownik_biura.class);
		pQuery.setParameter("id", idPrac);

		Pracownik_biura p = pQuery.getSingleResult();

		TypedQuery<Ogloszenie> oQuery = em.createQuery(
				"SELECT w FROM Ogloszenie w WHERE w.id_ogloszenia=:id", Ogloszenie.class);
		oQuery.setParameter("id", idOgl);
		
		Ogloszenie o = oQuery.getSingleResult();
		
		List<Ogloszenie> oList = p.getOgloszenie();
		oList.add(o);
		p.setOgloszenie(oList);;
		o.setPracownik(p);
		System.out.println("aaa");
	}

}
