package com.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.models.Klient_poszukujacy_pracy;
import com.models.Klient_pracodawca;
import com.models.Ogloszenie;

/**
 * Session Bean implementation class OgloszenieService
 */
@Stateless
@LocalBean
public class OgloszenieService {

	/**
	 * Default constructor.
	 */
	public OgloszenieService() {
		// TODO Auto-generated constructor stub
	}

	@PersistenceContext(unitName = "BiuroPosrednictwaPracyBCC")
	EntityManager em;

	public void dodajOgloszenie(Ogloszenie o) {
		em.persist(o);
	}

	public List<Ogloszenie> getOgloszeniaWszystkie() {
		TypedQuery<Ogloszenie> query = em.createQuery("SELECT o from Ogloszenie o", Ogloszenie.class);
		List<Ogloszenie> wynik = query.getResultList();
		return wynik;
	}

	public List<Ogloszenie> getOgloszeniaWszystkie(Integer strona, Integer liczbaOgloszenNaStrone) {
		TypedQuery<Ogloszenie> query = em.createQuery("SELECT o from Ogloszenie o", Ogloszenie.class);
		query.setFirstResult((strona-1) * liczbaOgloszenNaStrone);
		query.setMaxResults(liczbaOgloszenNaStrone);
		List<Ogloszenie> wynik = query.getResultList();
		return wynik;
	}
	
	public List<Ogloszenie> getOgloszeniaZatwierdzone(Integer strona, Integer liczbaOgloszenNaStrone) {
		TypedQuery<Ogloszenie> query = em.createQuery("SELECT o FROM Ogloszenie o WHERE o.pracownik IS NOT NULL AND o.terminWaznosciOgl>CURRENT_DATE", Ogloszenie.class);
		query.setFirstResult((strona-1) * liczbaOgloszenNaStrone);
		query.setMaxResults(liczbaOgloszenNaStrone);
		List<Ogloszenie> wynik = query.getResultList();
		return wynik;
	}
	
	public List<Ogloszenie> getOgloszeniaNiezatwierdzone(Integer strona, Integer liczbaOgloszenNaStrone) {
		TypedQuery<Ogloszenie> query = em.createQuery("SELECT o FROM Ogloszenie o WHERE o.pracownik IS NULL", Ogloszenie.class);
		query.setFirstResult((strona-1) * liczbaOgloszenNaStrone);
		query.setMaxResults(liczbaOgloszenNaStrone);
		List<Ogloszenie> wynik = query.getResultList();
		return wynik;
	}
	
	public Ogloszenie getOgloszenie(Integer idOgloszenia) {
		TypedQuery<Ogloszenie> query = em.createQuery("SELECT o FROM Ogloszenie o WHERE o.id_ogloszenia=:id_ogloszenia", Ogloszenie.class);
		query.setParameter("id_ogloszenia", idOgloszenia);
		return query.getSingleResult();
	}
	
	public Integer getLiczbaOgloszenWszystkich() {
		return ((Number)em.createQuery("SELECT COUNT(o.id_ogloszenia) FROM Ogloszenie o").getSingleResult()).intValue();
	}
	
	public Integer getLiczbaOgloszenZatwierdzonych() {
		return ((Number)em.createQuery("SELECT COUNT(o.id_ogloszenia) FROM Ogloszenie o WHERE o.pracownik IS NOT NULL AND o.terminWaznosciOgl>CURRENT_DATE").getSingleResult()).intValue();
		}
	
	public Integer getLiczbaOgloszenNiezatwierdzonych() {
		return ((Number)em.createQuery("SELECT COUNT(o.id_ogloszenia) FROM Ogloszenie o WHERE o.pracownik IS NULL").getSingleResult()).intValue();
	}
	
	public List<Ogloszenie> getOgloszeniaWyszukane(String branza, String miasto) {
		if(branza.length()==0){
			TypedQuery<Ogloszenie> query = em.createQuery("SELECT o FROM Ogloszenie o WHERE o.miasto=:wysz_miasto AND o.pracownik IS NOT NULL AND o.terminWaznosciOgl>CURRENT_DATE", Ogloszenie.class);
			query.setParameter("wysz_miasto", miasto);
			List<Ogloszenie> wynik = query.getResultList();
			return wynik;	
		}
		else if (miasto.length()==0){
			TypedQuery<Ogloszenie> query = em.createQuery("SELECT o FROM Ogloszenie o WHERE o.branza LIKE :wysz_branza AND o.pracownik IS NOT NULL AND o.terminWaznosciOgl>CURRENT_DATE", Ogloszenie.class);
			query.setParameter("wysz_branza","%"+branza+"%");
			List<Ogloszenie> wynik = query.getResultList();
			return wynik;	
		}
		else {
		TypedQuery<Ogloszenie> query = em.createQuery("SELECT o FROM Ogloszenie o WHERE (o.branza=:wysz_branza and o.miasto=:wysz_miasto AND o.pracownik IS NOT NULL AND o.terminWaznosciOgl>CURRENT_DATE)", Ogloszenie.class);
		query.setParameter("wysz_branza", branza);
		query.setParameter("wysz_miasto", miasto);
		List<Ogloszenie> wynik = query.getResultList();
		return wynik;
		}
	  }
	public Integer getLiczbaOgloszenWyszukanych(String miasto, String branza) {
		if (branza.length() == 0) {
			Query q = em.createQuery("SELECT COUNT(o.id_ogloszenia) FROM Ogloszenie o WHERE o.miasto=:miasto AND o.pracownik IS NOT NULL AND o.terminWaznosciOgl>CURRENT_DATE");
			q.setParameter("miasto", miasto);
			return ((Number) q.getSingleResult()).intValue();
		} else if (miasto.length() == 0) {
			Query q = em.createQuery("SELECT COUNT(o.id_ogloszenia) FROM Ogloszenie o WHERE o.branza LIKE :branza AND o.pracownik IS NOT NULL AND o.terminWaznosciOgl>CURRENT_DATE");
			q.setParameter("branza", "%"+branza+"%");
			return ((Number) q.getSingleResult()).intValue();
		} else {
			Query q = em.createQuery(
					"SELECT COUNT(o.id_ogloszenia) FROM Ogloszenie o WHERE o.miasto=:miasto AND o.branza LIKE :branza AND o.pracownik IS NOT NULL AND o.terminWaznosciOgl>CURRENT_DATE");
			q.setParameter("miasto", miasto);
			q.setParameter("branza", "%"+branza+"%");
			return ((Number) q.getSingleResult()).intValue();
		}
	}

	public void dodajOgloszenieDoPracodawcy(Integer idOgl, Integer idPrac) {
		TypedQuery<Klient_pracodawca> pQuery = em.createQuery(
				"SELECT p FROM Klient_pracodawca p WHERE p.id_pracodawcy=:id_pracodawcy", Klient_pracodawca.class);
		pQuery.setParameter("id_pracodawcy", idPrac);

		Klient_pracodawca k = pQuery.getSingleResult();

		TypedQuery<Ogloszenie> oQuery = em
				.createQuery("SELECT o FROM Ogloszenie o WHERE o.id_ogloszenia=:id_ogloszenia", Ogloszenie.class);
		oQuery.setParameter("id_ogloszenia", idOgl);

		Ogloszenie o = oQuery.getSingleResult();

		List<Ogloszenie> oList = k.getOgloszenie();
		oList.add(o);
		k.setOgloszenie(oList);
		o.setKlient(k);

	}
	
	public void usunOgloszenie(Integer idOgloszenia){
		Query q2 = em.createQuery("DELETE FROM Ogloszenie o WHERE o.id_ogloszenia=:id_ogloszenia");
		q2.setParameter("id_ogloszenia", idOgloszenia);
		q2.executeUpdate();	
	}
	
	public void archwizujOgloszenie(Ogloszenie o)
	{
		Query q = em.createQuery("Update Ogloszenie SET terminWaznosciOgl=:terminWaznosciOgl where id_ogloszenia=:id" );
		q.setParameter("terminWaznosciOgl", o.getTerminWaznosciOgl());
		q.setParameter("id", o.getId_ogloszenia());
		q.executeUpdate();
	}

	
	public Boolean czyZaaplikowane(Integer idOgl, Klient_poszukujacy_pracy k){
		TypedQuery<Ogloszenie> pQuery = em.createQuery(
				"SELECT o FROM Ogloszenie o WHERE o.id_ogloszenia=:id and o.klient_poszukujacy=:k", Ogloszenie.class);
		pQuery.setParameter("id", idOgl);
		pQuery.setParameter("k", k);
		
		if(pQuery.getResultList().size()==1) return true;
		return false;

	}
	

	public void edytujOgloszenie(Ogloszenie o)
	{
		Query q = em.createQuery("UPDATE Ogloszenie SET branza=:branza, miasto=:miasto, przedzialWiekowy=:przedzialWiekowy, poziomWyksztalcenia=:poziomWyksztalcenia, "
				+ "doswiadczenie=:doswiadczenie, opisStanowiska=:opisStanowiska, nazwaFirmy=:nazwaFirmy, adres=:adres, kraj=:kraj, region=:region, terminWaznosciOgl=:terminWaznosciOgl where id_ogloszenia=:id");
		
		q.setParameter("id", o.getId_ogloszenia());
		q.setParameter("branza", o.getBranza());
		q.setParameter("miasto", o.getMiasto());
		q.setParameter("przedzialWiekowy", o.getPrzedzialWiekowy());
		q.setParameter("poziomWyksztalcenia", o.getPoziomWyksztalcenia());
		q.setParameter("doswiadczenie", o.getDoswiadczenie());
		q.setParameter("opisStanowiska", o.getOpisStanowiska());
		q.setParameter("nazwaFirmy", o.getNazwaFirmy());
		q.setParameter("adres", o.getAdres());
		q.setParameter("kraj", o.getKraj());
		q.setParameter("region", o.getRegion());
		q.setParameter("terminWaznosciOgl", o.getTerminWaznosciOgl());
		q.executeUpdate();
	}
	
	// Zwraca id pracownika, kt�ry zamie�ci� og�oszenie
	public Integer getCzyjeOgloszenie(Integer idOgloszenia) {
		return getOgloszenie(idOgloszenia).getPracownik().getId_pracownika();
	}
	
}
