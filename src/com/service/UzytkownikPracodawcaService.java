package com.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.models.Klient_pracodawca;
import com.models.Ogloszenie;

/**
 * Session Bean implementation class UzytkownikPracodawcaService
 */
@Stateless
@LocalBean
public class UzytkownikPracodawcaService {

	@PersistenceContext(unitName = "BiuroPosrednictwaPracyBCC")
	EntityManager em;

	@EJB
	OgloszenieService os;
	
	/**
	 * Default constructor.
	 */
	public UzytkownikPracodawcaService() {
		// TODO Auto-generated constructor stub
	}

	public void dodajUzytkownika(Klient_pracodawca klient) {
		em.persist(klient);
	}

	public boolean czyLoginZajety(String login) {
		TypedQuery<Klient_pracodawca> zapytanie = em.createQuery("SELECT k from Klient_pracodawca k WHERE k.login=:login",
				Klient_pracodawca.class);

		zapytanie.setParameter("login", login);
		List<Klient_pracodawca> lista = zapytanie.getResultList();

		if (lista.size()==1) return true;
		else return false;
	}
	
	public Klient_pracodawca pobierzDanePracodawcy(Integer id)
	{
		TypedQuery<Klient_pracodawca> zapytanie = em.createQuery("SELECT k from Klient_pracodawca k WHERE k.id_pracodawcy=:id",
				Klient_pracodawca.class);
		
		zapytanie.setParameter("id", id);
		Klient_pracodawca k = zapytanie.getSingleResult();
		
		return k;						
	}
	
	public void edytujDanePracodawcy(Klient_pracodawca k)
	{
		Query q = em.createQuery("UPDATE Klient_pracodawca SET haslo=:haslo, email=:email, nrTel=:nrTel, nazwa_firmy=:nazwa_firmy, adres=:adres, kraj=:kraj, region=:region where id_pracodawcy=:id");
		q.setParameter("id", k.getId_pracodawcy());
		q.setParameter("haslo", k.getHaslo());
		q.setParameter("email", k.getEmail());
		q.setParameter("nrTel", k.getNrTel());
		q.setParameter("nazwa_firmy", k.getNazwa_firmy());
		q.setParameter("adres", k.getAdres());
		q.setParameter("kraj", k.getKraj());
		q.setParameter("region", k.getRegion());
		q.executeUpdate();
		
	}
	
	public void usunKontoPracodawcy(Integer id)
	{
		// Najpierw usuni�cie og�osze� pracodawcy:
		Klient_pracodawca pracodawca = pobierzDanePracodawcy(id);
		List<Ogloszenie> ogloszenia = pracodawca.getOgloszenie();
		
		for(Ogloszenie ogloszenie:ogloszenia) {
			os.usunOgloszenie(ogloszenie.getId_ogloszenia());
		}
		
		// Na koniec usuni�cie konta pracodawcy:
		Query q = em.createQuery("DELETE FROM Klient_pracodawca k WHERE k.id_pracodawcy=:id");
		q.setParameter("id", id);
		q.executeUpdate();
	}

	public Integer getLiczbaUzytkownikowPracodawcow() {
	    return ((Number)em.createQuery("SELECT COUNT(k.id_pracodawcy) FROM Klient_pracodawca k ").getSingleResult()).intValue();
	    }
	
}
