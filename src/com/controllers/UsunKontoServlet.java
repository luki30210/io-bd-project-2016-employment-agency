package com.controllers;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.service.UzytkownikPoszukujacyPracyService;
import com.service.UzytkownikPracodawcaService;

/**
 * Servlet implementation class UsunKontoServlet
 */
@WebServlet("/UsunKonto")
public class UsunKontoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	UzytkownikPoszukujacyPracyService u_poszukujacy;

	@EJB
	UzytkownikPracodawcaService u_pracodawca;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UsunKontoServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");

		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("poszukujacy") == 0
					|| request.getSession().getAttribute("typ").toString().compareTo("pracodawca") == 0) {
				RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
				RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/UsunKonto.jsp");
				RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

				viewNaglowek.include(request, response);
				viewTresc.include(request, response);
				viewStopka.include(request, response);
			} else
				response.sendRedirect("");
		} else
			response.sendRedirect("");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");

		Object typ = request.getSession().getAttribute("typ");
		Integer id = (Integer)request.getSession().getAttribute("id");

		if (typ.toString().compareTo("poszukujacy") == 0) {
			u_poszukujacy.usunKontoPoszukujacego(id);
		} else if (typ.toString().compareTo("pracodawca") == 0) {
			u_pracodawca.usunKontoPracodawcy(id);
		}
		response.sendRedirect("LogoutServlet");
	}
}