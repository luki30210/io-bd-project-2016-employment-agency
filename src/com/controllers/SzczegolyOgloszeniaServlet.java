package com.controllers;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Klient_poszukujacy_pracy;
import com.models.Ogloszenie;
import com.service.OgloszenieService;
import com.service.UzytkownikPoszukujacyPracyService;

/**
 * Servlet implementation class SzczegolyOgloszeniaServlet
 */
@WebServlet("/SzczegolyOgloszenia")
public class SzczegolyOgloszeniaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	OgloszenieService os;

	@EJB
	UzytkownikPoszukujacyPracyService us;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SzczegolyOgloszeniaServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");

		Integer idOgloszenia = Integer.parseInt(request.getParameter("id_ogloszenia"));
		Ogloszenie ogloszenie = os.getOgloszenie(idOgloszenia);
		request.setAttribute("ogloszenie", ogloszenie);

		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("poszukujacy") == 0) {
				Integer idPoszukujacego = (Integer) request.getSession().getAttribute("id");
				Klient_poszukujacy_pracy k = us.pobierzDanePoszukujacego(idPoszukujacego);

				if (os.czyZaaplikowane(idOgloszenia, k)) {
					request.setAttribute("zaaplikowane", true);
				} else
					request.setAttribute("zaaplikowane", false);
			}
		}
		RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
		RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/StronaGlowna.jsp");
		RequestDispatcher viewSzczegolyOgloszenia = request
				.getRequestDispatcher("WEB-INF/views/SzczegolyOgloszenia.jsp");
		RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

		viewNaglowek.include(request, response);
		viewTresc.include(request, response);
		viewSzczegolyOgloszenia.include(request, response);
		viewStopka.include(request, response);
	}

}
