package com.controllers;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Ogloszenie;
import com.service.OgloszenieService;

/**
 * Servlet implementation class StronaGlownaServlet
 */
@WebServlet("")
public class StronaGlownaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	OgloszenieService os;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StronaGlownaServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
		//request.setCharacterEncoding("UTF-8");
	    request.setAttribute("obecna_strona", "strona_glowna");
	    
		request.setAttribute("branza", "");
		request.setAttribute("miasto", "");
		request.setAttribute("stronaNastepna", false);
	    request.setAttribute("stronaPoprzednia", false);
		
		Integer strona = 1;
		Integer liczbaOgloszenNaStrone = 20;
		
		Integer liczbaOgloszen = os.getLiczbaOgloszenZatwierdzonych();
		
		request.setAttribute("liczba_ogloszen", liczbaOgloszen);
		request.setAttribute("liczba_ogloszen_na_strone", liczbaOgloszenNaStrone);
		request.setAttribute("strona", strona);
		
		List<Ogloszenie> listaOgloszen = (List<Ogloszenie>) os.getOgloszeniaZatwierdzone(strona, liczbaOgloszenNaStrone);
		request.setAttribute("lista_ogloszen", listaOgloszen);
		
		RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
		RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/StronaGlowna.jsp");
		RequestDispatcher viewOgloszenia = request.getRequestDispatcher("WEB-INF/views/WyswietlOgloszenia.jsp");
		RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

		viewNaglowek.include(request, response);
		viewTresc.include(request, response);
		viewOgloszenia.include(request, response);
		viewStopka.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*response.setContentType("text/html; charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
	    
	    Integer liczbaOgloszenNaStrone = (Integer)request.getAttribute("liczba_ogloszen_na_strone");
	    Integer strona = (Integer)request.getAttribute("strona");
	    //Integer liczbaOgloszen = (Integer)request.getAttribute("liczba_ogloszen");
	    
	    if ((Boolean) request.getAttribute("stronaNastepna")) {
	    	strona++;
	    } else if ((Boolean) request.getAttribute("stronaPoprzednia")) {
	    	strona--;
	    }
	    
	    System.out.println(strona);
	    	
	    List<Ogloszenie> listaOgloszen = (List<Ogloszenie>) os.getOgloszenia(1, 20);
		request.setAttribute("lista_ogloszen", listaOgloszen);
	    
	   // request.setAttribute("strona", strona);
	    request.setAttribute("stronaNastepna", false);
	    request.setAttribute("stronaPoprzednia", false);
	    */
		
		response.setContentType("text/html; charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
	    request.setAttribute("obecna_strona", "strona_glowna");
	    
	    Integer strona = Integer.parseInt(request.getParameter("strona"));
	    System.out.println(strona);
		Integer liczbaOgloszenNaStrone = 20;	//(Integer)request.getAttribute("liczba_ogloszen_na_strone")
	    
	    Integer liczbaOgloszen = os.getLiczbaOgloszenZatwierdzonych();
	    
	    request.setAttribute("liczba_ogloszen", liczbaOgloszen);
		request.setAttribute("liczba_ogloszen_na_strone", liczbaOgloszenNaStrone);
		request.setAttribute("strona", strona);
		
		List<Ogloszenie> listaOgloszen = (List<Ogloszenie>) os.getOgloszeniaZatwierdzone(strona, liczbaOgloszenNaStrone);
		request.setAttribute("lista_ogloszen", listaOgloszen);
		
		RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
		RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/StronaGlowna.jsp");
		RequestDispatcher viewOgloszenia = request.getRequestDispatcher("WEB-INF/views/WyswietlOgloszenia.jsp");
		RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

		viewNaglowek.include(request, response);
		viewTresc.include(request, response);
		viewOgloszenia.include(request, response);
		viewStopka.include(request, response);
	}

}
