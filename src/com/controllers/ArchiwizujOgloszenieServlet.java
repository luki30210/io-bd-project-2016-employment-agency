package com.controllers;

import java.io.IOException;
import java.util.Date;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Ogloszenie;
import com.service.OgloszenieService;

/**
 * Servlet implementation class ArchiwizujOgloszenie
 */
@WebServlet("/ArchiwizujOgloszenie")
public class ArchiwizujOgloszenieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ArchiwizujOgloszenieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    @EJB
    OgloszenieService os;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		Integer idOgloszenia = Integer.parseInt(request.getParameter("id_ogloszenia"));
		//ustalenie 'dzisiejszej' daty - konstruktor ustawia date z ktora sie wywoluje
		Date d = new Date();
		
		Ogloszenie o = os.getOgloszenie(idOgloszenia);
		o.setTerminWaznosciOgl(d);
		
		os.archwizujOgloszenie(o);
		
		response.sendRedirect("MojeOgloszenia");
		
	}

}
