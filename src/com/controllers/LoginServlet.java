package com.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.models.rodzaj_konta;
import com.service.LoginService;
import com.service.OdwiedzinyService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// polaczenie z serwisem i baza
	@EJB
	LoginService ps;

	@EJB
	OdwiedzinyService o;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("obecna_strona", "logowanie");
		
		if (request.getSession().getAttribute("uzytkownik") == null) {
			response.setContentType("text/html; charset=UTF-8");
		    response.setCharacterEncoding("UTF-8");
		    
			RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
			RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/loginPage.jsp");
			RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");
	
			viewNaglowek.include(request, response);
			viewTresc.include(request, response);
			viewStopka.include(request, response);
		} else {
			response.sendRedirect("");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		o.dodaj();
		
		response.setContentType("text/html; charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
	    request.setCharacterEncoding("UTF-8");
	    
	    request.setAttribute("obecna_strona", "logowanie");
	    
		// pobranie parametrow wpisanych na stronie przez uzytkownika
		String uzytkownik = request.getParameter("uzytkownik");
		String haslo = request.getParameter("hasloPwd");

		Integer a = ps.porownajLoginPracodawcy(uzytkownik, haslo);
		Integer b = ps.porownajLoginPoszukujacego(uzytkownik, haslo);
		Integer c = ps.porownajLoginPracownika(uzytkownik, haslo);
		// sprawdzenie czy podane haslo i nazwa sa poprawne
		if (a != 0 || b != 0 || c != 0) {
			// wprowadzenie zmiennej sesji
			HttpSession session = request.getSession();

			if (a != 0) {
				session.setAttribute("typ", "pracodawca");
				session.setAttribute("id", a);
			} else if (b != 0) {
				session.setAttribute("typ", "poszukujacy");
				session.setAttribute("id", b);
			} else {
				session.setAttribute("typ", "pracownik");
				session.setAttribute("id", c);
				 if (ps.getRodzajKontaPracownikaBiura(c) == rodzaj_konta.administrator) {
			          session.setAttribute("administrator", true);
			        }
			}

			session.setAttribute("uzytkownik", uzytkownik);
			// sesja wygasa po 30min
			session.setMaxInactiveInterval(30 * 60);
			
			/*
			Cookie loginCookie = new Cookie("uzytkownik", uzytkownik);
			loginCookie.setMaxAge(30 * 60);
			response.addCookie(loginCookie);
			*/
			
			RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
			RequestDispatcher viewPowitanie = request.getRequestDispatcher("WEB-INF/views/LoginSukces.jsp");
			//RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/StronaGlowna.jsp");
			//RequestDispatcher viewOgloszenia = request.getRequestDispatcher("WEB-INF/views/WyswietlOgloszenia.jsp");
			RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");
			viewNaglowek.include(request, response);
			viewPowitanie.include(request, response);
			//viewTresc.include(request, response);
			//viewOgloszenia.include(request, response);
			viewStopka.include(request, response);
		} else {
			RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
			RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/loginPage.jsp");
			RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

			viewNaglowek.include(request, response);

			PrintWriter out = response.getWriter();
			out.println("<font color=red>Niepoprawne dane logowania.</font>");

			viewTresc.include(request, response);
			viewStopka.include(request, response);
		}
	}
	
	
}
