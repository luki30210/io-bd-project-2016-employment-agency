package com.controllers;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Ogloszenie;
import com.service.OgloszenieService;

/**
 * Servlet implementation class PanelPracownikaServlet
 */
@WebServlet("/PanelPracownika")
public class PanelPracownikaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	OgloszenieService os;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PanelPracownikaServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("pracownik") != 0)
				response.sendRedirect("");
		} else
			response.sendRedirect("");
		response.setContentType("text/html; charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
	    
		Integer strona = 1;
		Integer liczbaOgloszenNaStrone = 20;
		
		Integer liczbaOgloszen = os.getLiczbaOgloszenNiezatwierdzonych();
		
		request.setAttribute("liczba_ogloszen", liczbaOgloszen);
		request.setAttribute("liczba_ogloszen_na_strone", liczbaOgloszenNaStrone);
		request.setAttribute("strona", strona);
		
		List<Ogloszenie> listaOgloszen = (List<Ogloszenie>) os.getOgloszeniaNiezatwierdzone(strona, liczbaOgloszenNaStrone);
		request.setAttribute("lista_ogloszen", listaOgloszen);
		
		RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
		RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/StronaGlowna.jsp");
		RequestDispatcher viewOgloszenia = request.getRequestDispatcher("WEB-INF/views/WyswietlOgloszenia.jsp");
		RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

		viewNaglowek.include(request, response);
		viewTresc.include(request, response);
		viewOgloszenia.include(request, response);
		viewStopka.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
