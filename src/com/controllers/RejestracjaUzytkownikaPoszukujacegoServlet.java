package com.controllers;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Klient_poszukujacy_pracy;
import com.models.plec;
import com.service.PBKDF2;
import com.service.UzytkownikPoszukujacyPracyService;
import com.service.UzytkownikPracodawcaService;

/**
 * Servlet implementation class RejestracjaUzytkownikaPoszukujacegoServlet
 */
@WebServlet("/Rejestracja1") // @WebServlet("/RejestracjaUzytkownikaPoszukujacegoServlet")
public class RejestracjaUzytkownikaPoszukujacegoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	UzytkownikPoszukujacyPracyService uzytkownikPoszukujacyPracy;
	
	@EJB
	UzytkownikPracodawcaService uzytkownikPracodawca;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RejestracjaUzytkownikaPoszukujacegoServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("uzytkownik") == null) {
			response.setContentType("text/html; charset=UTF-8");
		    response.setCharacterEncoding("UTF-8");
		    
		    request.setAttribute("obecna_strona", "rejestracja1");
		    
			request.setAttribute("login_atrybut", "");
			request.setAttribute("email_atrybut", "");
			request.setAttribute("imie_atrybut", "");
			request.setAttribute("nazwisko_atrybut", "");
			request.setAttribute("data_urodzenia_atrybut", "");
			request.setAttribute("nr_tel_atrybut", "");
	
			RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
			RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/RejestracjaUzytkownikaPoszukujacego.jsp");
			RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");
	
			viewNaglowek.include(request, response);
			viewTresc.include(request, response);
			viewStopka.include(request, response);
		} else {
			response.sendRedirect("");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html; charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
	    
	    request.setAttribute("obecna_strona", "rejestracja1");
	    
		request.setAttribute("bledy", false);
		request.setCharacterEncoding("UTF-8"); // dzięki temu, dane pobierane z
												// formularza będą prawidłowo
												// odkodowywane

		Klient_poszukujacy_pracy klient = new Klient_poszukujacy_pracy();

		/* --------------------------- LOGIN ---------------------------- */
		String login = request.getParameter("login");
		if (login.length() == 0) {
			request.setAttribute("bledy", true);
			request.setAttribute("login_blad_brak", true);
			request.setAttribute("login_atrybut", "");
		} else if (uzytkownikPoszukujacyPracy.czyLoginZajety(login) || uzytkownikPracodawca.czyLoginZajety(login)) {
			// ZROBIĆ: SPRAWDZANIE CZY LOGIN JEST ZAJĘTY PRZEZ DYREKTORA LUB ADMINISTRATORA
			request.setAttribute("bledy", true);
			request.setAttribute("login_blad_zajety", true);
			request.setAttribute("login_atrybut", login);
		} else {
			klient.setLogin(login);
			request.setAttribute("login_atrybut", login);
		}

		/* --------------------------- HASŁO ---------------------------- */
		String haslo = request.getParameter("haslo");
		if (haslo.length() == 0) {
			request.setAttribute("bledy", true);
			request.setAttribute("haslo_blad", true);
		} else {
			try {
				klient.setHaslo(PBKDF2.generateStorngPasswordHash(haslo));
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				request.setAttribute("bledy", true); // ???
				request.setAttribute("haslo_blad", true);
				e.printStackTrace();
			}
		}

		/* --------------------------- E-MAIL --------------------------- */
		String email = request.getParameter("email");
		Pattern wzorAdresuEmail = Pattern.compile(
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		Matcher dopasowanieAdresuEmail = wzorAdresuEmail.matcher(email);
		if (email.length() == 0) {
			request.setAttribute("bledy", true);
			request.setAttribute("email_blad_brak", true);
			request.setAttribute("email_atrybut", "");
		} else if (dopasowanieAdresuEmail.find()) {
			klient.setEmail(email);
			request.setAttribute("email_atrybut", email);
		} else {
			request.setAttribute("bledy", true);
			request.setAttribute("email_blad_wzor", true);
			request.setAttribute("email_atrybut", "");
		}

		/* ---------------------------- IMIĘ ---------------------------- */
		String imie = request.getParameter("imie");
		if (imie.length() == 0) {
			// System.out.println("Imię nie zostało podane");
			request.setAttribute("bledy", true);
			request.setAttribute("imie_blad", true);
			request.setAttribute("imie_atrybut", "");
		} else {
			klient.setImie(imie);
			request.setAttribute("imie_atrybut", imie);
		}

		/* -------------------------- NAZWISKO -------------------------- */
		String nazwisko = request.getParameter("nazwisko");
		if (nazwisko.length() == 0) {
			// System.out.println("Nazwisko nie zostało podane");
			request.setAttribute("bledy", true);
			request.setAttribute("nazwisko_blad", true);
			request.setAttribute("nazwisko_atrybut", "");
		} else {
			klient.setNazwisko(nazwisko);
			request.setAttribute("nazwisko_atrybut", nazwisko);
		}

		/* ---------------------------- PŁEĆ ---------------------------- */
		String plecFormularz = request.getParameter("plec");
		klient.setPlec(plec.valueOf(plecFormularz));
		if (klient.getPlec() == plec.kobieta) {
			request.setAttribute("plec_kobieta", true);
		}

		/* ----------------------- DATA URODZENIA ----------------------- */
		String dataUrodzeniaString = request.getParameter("data_urodzenia");
		String wzorDaty = "^(\\d{4}-\\d{1,2}-\\d{1,2})$";
		Pattern r = Pattern.compile(wzorDaty);
		Matcher m = r.matcher(dataUrodzeniaString);
		if (m.find()) {
			String dataUrodzeniaArray[] = dataUrodzeniaString.split("-");
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dataUrodzeniaArray[2]));
			calendar.set(Calendar.MONTH, Integer.parseInt(dataUrodzeniaArray[1]) - 1);
			calendar.set(Calendar.YEAR, Integer.parseInt(dataUrodzeniaArray[0]));
			klient.setDataUrodzenia(calendar.getTime());
			request.setAttribute("data_urodzenia_atrybut", dataUrodzeniaString);
		} else {
			request.setAttribute("bledy", true);
			request.setAttribute("data_urodzenia_blad", true);
		}

		/* ----------------------- NUMER TELEFONU ----------------------- */
		String nrTelefonu = request.getParameter("nr_tel");
		if (nrTelefonu.length() == 9) {
			klient.setNrTel(nrTelefonu);
			request.setAttribute("nr_tel_atrybut", nrTelefonu);
		} else {
			request.setAttribute("bledy", true);
			request.setAttribute("nr_tel_blad", true);
			request.setAttribute("nr_tel_atrybut", "");
		}

		/* ---------------------- DATA REJESTRACJI ---------------------- */
		klient.setDataRejestracji(Calendar.getInstance().getTime());

		/* ---------------------- OBECNOŚĆ BŁĘDÓW ----------------------- */
		if ((Boolean) request.getAttribute("bledy")) {
			RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
			RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/RejestracjaUzytkownikaPoszukujacego.jsp");
			RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

			viewNaglowek.include(request, response);
			viewTresc.include(request, response);
			viewStopka.include(request, response);
		} else {
			synchronized (this) {
				uzytkownikPoszukujacyPracy.dodajUzytkownika(klient);
			}
			response.sendRedirect(""); // Przekierowanie na stronę główną
		}
	}

}
