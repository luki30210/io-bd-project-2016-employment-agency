package com.controllers;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.service.PracownikBiuraService;

/**
 * Servlet implementation class ZatwierdzOgloszenieServlet
 */
@WebServlet("/ZatwierdzOgloszenie")
public class ZatwierdzOgloszenieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	PracownikBiuraService ps;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ZatwierdzOgloszenieServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer idPracownika= (Integer)request.getSession().getAttribute("id");
		Integer idOgloszenia = Integer.parseInt(request.getParameter("id_ogloszenia"));
		
		ps.zaakceptujOgloszenie(idOgloszenia, idPracownika);
		response.sendRedirect("PanelPracownika");
	}

}
