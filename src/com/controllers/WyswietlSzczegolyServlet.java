package com.controllers;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Doswiadczenie;
import com.models.Klient_poszukujacy_pracy;
import com.models.Wyksztalcenie;
import com.service.DoswiadczenieService;
import com.service.UzytkownikPoszukujacyPracyService;
import com.service.WyksztalcenieService;

/**
 * Servlet implementation class WyswietlDoswiadczenieServlet
 */
@WebServlet("/WyswietlSzczegoly")
public class WyswietlSzczegolyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	@EJB
	DoswiadczenieService os;

	@EJB
	WyksztalcenieService ws;

	@EJB
	UzytkownikPoszukujacyPracyService us;

	public WyswietlSzczegolyServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("poszukujacy") == 0) {
				Integer id = (Integer) request.getSession().getAttribute("id");

				List<Doswiadczenie> dList = (List<Doswiadczenie>) os.wyswietlDoswiadczenie(id);
				request.setAttribute("lista_doswiadczen", dList);

				List<Wyksztalcenie> wList = (List<Wyksztalcenie>) ws.wyswietlWyksztalcenie(id);
				request.setAttribute("lista_wyksztalcenie", wList);

				Klient_poszukujacy_pracy k = us.pobierzDanePoszukujacego(id);
				request.setAttribute("dane_poszukujacego", k);

				RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
				RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/StronaGlowna.jsp");
				RequestDispatcher viewSzczegoly = request
						.getRequestDispatcher("WEB-INF/views/WyswietlSzczegolyKonta.jsp");
				RequestDispatcher viewWyksztalcenie = request
						.getRequestDispatcher("WEB-INF/views/WyswietlWyksztalcenie.jsp");
				RequestDispatcher viewDoswiadczenie = request
						.getRequestDispatcher("WEB-INF/views/WyswietlDoswiadczenie.jsp");
				RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

				viewNaglowek.include(request, response);
				viewTresc.include(request, response);
				viewSzczegoly.include(request, response);
				viewWyksztalcenie.include(request, response);
				viewDoswiadczenie.include(request, response);
				viewStopka.include(request, response);
			} else
				response.sendRedirect("");
		} else
			response.sendRedirect("");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");

		Integer id = Integer.parseInt(request.getParameter("id_klienta"));

		List<Doswiadczenie> dList = (List<Doswiadczenie>) os.wyswietlDoswiadczenie(id);
		request.setAttribute("lista_doswiadczen", dList);

		List<Wyksztalcenie> wList = (List<Wyksztalcenie>) ws.wyswietlWyksztalcenie(id);
		request.setAttribute("lista_wyksztalcenie", wList);

		Klient_poszukujacy_pracy k = us.pobierzDanePoszukujacego(id);
		request.setAttribute("dane_poszukujacego", k);

		RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
		RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/StronaGlowna.jsp");
		RequestDispatcher viewSzczegoly = request.getRequestDispatcher("WEB-INF/views/WyswietlSzczegolyKonta.jsp");
		RequestDispatcher viewWyksztalcenie = request.getRequestDispatcher("WEB-INF/views/WyswietlWyksztalcenie.jsp");
		RequestDispatcher viewDoswiadczenie = request.getRequestDispatcher("WEB-INF/views/WyswietlDoswiadczenie.jsp");
		RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

		viewNaglowek.include(request, response);
		viewTresc.include(request, response);
		viewSzczegoly.include(request, response);
		viewWyksztalcenie.include(request, response);
		viewDoswiadczenie.include(request, response);
		viewStopka.include(request, response);
	}

}
