package com.controllers;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.service.UzytkownikPoszukujacyPracyService;

/**
 * Servlet implementation class AplikacjaNaOgloszenie
 */
@WebServlet("/AplikacjaNaOgloszenie")
public class AplikacjaNaOgloszenieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	UzytkownikPoszukujacyPracyService uzytkownik_posz;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AplikacjaNaOgloszenieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer idPoszukujacego= (Integer)request.getSession().getAttribute("id");
		Integer idOgloszenia = Integer.parseInt(request.getParameter("id_ogloszenia"));

		uzytkownik_posz.aplikacja(idOgloszenia, idPoszukujacego);
		response.sendRedirect("");
	}

}
