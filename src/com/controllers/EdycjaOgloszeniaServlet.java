package com.controllers;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Ogloszenie;
import com.models.poziom_szkoly;
import com.service.OgloszenieService;

/**
 * Servlet implementation class EdycjaOgloszeniaServlet
 */
@WebServlet("/EdycjaOgloszeniaServlet")
public class EdycjaOgloszeniaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EdycjaOgloszeniaServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@EJB
	OgloszenieService os;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.sendRedirect("");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8"); 

		if (request.getParameter("wyswietl")!= null) {
			RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
			RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/StronaGlowna.jsp");
			RequestDispatcher viewOgloszenie = request.getRequestDispatcher("WEB-INF/views/EdycjaOgloszenia.jsp");
			RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

			Integer idOgloszenia = Integer.parseInt(request.getParameter("id_ogloszenia"));
			Ogloszenie ogloszenie = os.getOgloszenie(idOgloszenia);
			request.setAttribute("id", idOgloszenia);

			/* -- Ustawienie aktualnych danych w formularzu -- */
			request.setAttribute("branza_atrybut", ogloszenie.getBranza());
			request.setAttribute("miasto_atrybut", ogloszenie.getMiasto());
			request.setAttribute("przedzialWiekowy_atrybut", ogloszenie.getPrzedzialWiekowy());
			request.setAttribute("doswiadczenie_atrybut", ogloszenie.getDoswiadczenie());
			request.setAttribute("opisStanowiska_atrybut", ogloszenie.getOpisStanowiska());
			request.setAttribute("nazwaFirmy_atrybut", ogloszenie.getNazwaFirmy());
			request.setAttribute("adres_atrybut", ogloszenie.getAdres());
			request.setAttribute("kraj_atrybut", ogloszenie.getKraj());
			request.setAttribute("region_atrybut", ogloszenie.getRegion());

			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			String formatted = format1.format(ogloszenie.getTerminWaznosciOgl());
			request.setAttribute("terminWaznosciOgl_atrybut", formatted);

			viewNaglowek.include(request, response);
			viewTresc.include(request, response);
			viewOgloszenie.include(request, response);
			viewStopka.include(request, response);
			
		} else if (request.getParameter("edytuj") != null) {
			Integer idOgloszenia = Integer.parseInt(request.getParameter("id_ogloszenia"));

			System.out.println(idOgloszenia);
			Ogloszenie ogloszenie = os.getOgloszenie(idOgloszenia);

			String branza = request.getParameter("branza");
			ogloszenie.setBranza(branza);

			String miasto = request.getParameter("miasto");
			ogloszenie.setMiasto(miasto);

			String przedzialWiekowy = request.getParameter("przedzialWiekowy");
			ogloszenie.setPrzedzialWiekowy(przedzialWiekowy);

			
			  String wyksztalcenieFormularz =
			  request.getParameter("poziomWyksztalcenia");
			  ogloszenie.setPoziomWyksztalcenia(poziom_szkoly.valueOf(
			  wyksztalcenieFormularz)); if (ogloszenie.getPoziomWyksztalcenia()
			  == poziom_szkoly.podstawowa) {
			  request.setAttribute("poziomWyksztalcenia_podstawowe", true); }
			  else if(ogloszenie.getPoziomWyksztalcenia() ==
			  poziom_szkoly.gimnazjalna) {
			  request.setAttribute("poziomWyksztalcenia_gimnazjalne", true); }
			  else if(ogloszenie.getPoziomWyksztalcenia() ==
			  poziom_szkoly.srednia){
			  request.setAttribute("poziomWyksztalcenia_zawodowe", true); }
			  else if(ogloszenie.getPoziomWyksztalcenia() ==
			  poziom_szkoly.zawodowa){
			  request.setAttribute("poziomWyksztalcenia_zawodowe2", true); }
			  else if(ogloszenie.getPoziomWyksztalcenia() ==
			  poziom_szkoly.wyzsza){
			  request.setAttribute("poziomWyksztalcenie_wyzsze", true); }
			 
			  /*
				 * String plecFormularz = request.getParameter("plec");
				 * klient.setPlec(plec.valueOf(plecFormularz)); if (klient.getPlec() ==
				 * plec.kobieta) { request.setAttribute("plec_kobieta", true); }
				 */

			String doswiadczenie = request.getParameter("doswiadczenie");
			ogloszenie.setDoswiadczenie(doswiadczenie);

			String opisStanowiska = request.getParameter("opisStanowiska");
			ogloszenie.setOpisStanowiska(opisStanowiska);

			String nazwaFirmy = request.getParameter("nazwaFirmy");
			ogloszenie.setNazwaFirmy(nazwaFirmy);

			String adres = request.getParameter("adres");
			ogloszenie.setAdres(adres);

			String kraj = request.getParameter("kraj");
			ogloszenie.setKraj(kraj);

			String region = request.getParameter("region");
			ogloszenie.setRegion(region);

			String terminWaznosciOgl = request.getParameter("terminWaznosciOgl");
			String wzorDaty = "^(\\d{4}-\\d{1,2}-\\d{1,2})$";
			Pattern r = Pattern.compile(wzorDaty);
			Matcher m = r.matcher(terminWaznosciOgl);
			if (m.find()) {
				String terminWaznosciOglArray[] = terminWaznosciOgl.split("-");
				Calendar calendar = Calendar.getInstance();
				calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(terminWaznosciOglArray[2]));
				calendar.set(Calendar.MONTH, Integer.parseInt(terminWaznosciOglArray[1]) - 1);
				calendar.set(Calendar.YEAR, Integer.parseInt(terminWaznosciOglArray[0]));
				ogloszenie.setTerminWaznosciOgl(calendar.getTime());
				request.setAttribute("terminWaznosciOgl_atrybut", terminWaznosciOgl);
			}
			
			 /* SimpleDateFormat terminWaznosciOgl = new SimpleDateFormat("yyyy-MM-dd"); 
			  String formatted = terminWaznosciOgl.format(ogloszenie.getTerminWaznosciOgl());
			  request.setAttribute("terminWaznosciOgl", formatted);
			 */
			os.edytujOgloszenie(ogloszenie);
			response.sendRedirect("");
		}
		// Integer idOgloszenia =
		// Integer.parseInt(request.getParameter("id_ogloszenia"));
		// Ogloszenie ogloszenie = os.getOgloszenie(idOgloszenia);

	}

}
