package com.controllers;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Klient_pracodawca;
import com.service.UzytkownikPracodawcaService;

/**
 * Servlet implementation class MojeOgloszeniaServlet
 */
@WebServlet("/MojeOgloszenia")
public class MojeOgloszeniaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	UzytkownikPracodawcaService us;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MojeOgloszeniaServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setAttribute("obecna_strona", "moje_ogloszenia");
		
		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("pracodawca") == 0) {
				Object value = request.getSession().getAttribute("id");
				Klient_pracodawca k = us.pobierzDanePracodawcy((Integer) value);

				Integer strona = 1;
				Integer liczbaOgloszenNaStrone = 20;

				Integer liczbaOgloszen = k.getOgloszenie().size();

				request.setAttribute("liczba_ogloszen", liczbaOgloszen);
				request.setAttribute("liczba_ogloszen_na_strone", liczbaOgloszenNaStrone);
				request.setAttribute("strona", strona);

				request.setAttribute("lista_ogloszen", k.getOgloszenie());

				RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
				RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/StronaGlowna.jsp");
				RequestDispatcher viewOgloszenia = request.getRequestDispatcher("WEB-INF/views/WyswietlOgloszenia.jsp");
				RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

				viewNaglowek.include(request, response);
				viewTresc.include(request, response);
				viewOgloszenia.include(request, response);
				viewStopka.include(request, response);
			}
			else response.sendRedirect("");

		} else
			response.sendRedirect("");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("");
	}

}
