package com.controllers;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Pracownik_biura;
import com.models.rodzaj_konta;
import com.service.PBKDF2;
import com.service.PracownikBiuraService;

/**
 * Servlet implementation class TworzenieNowychKontDlaAdminowServlet
 */
@WebServlet("/TworzenieNowychKontDlaAdminowServlet")
public class TworzenieNowychKontDlaAdminowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	@EJB
	PracownikBiuraService ps;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TworzenieNowychKontDlaAdminowServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Pracownik_biura p = new Pracownik_biura();
		p.setLogin("admin1");
		try {
			p.setHaslo(PBKDF2.generateStorngPasswordHash("admin1"));
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		} 
		p.setImie("Filip");
		p.setNazwisko("Komandor");
		p.setTyp_konta(rodzaj_konta.administrator);
		p.setEmail("filip@komandor.pl");
		ps.dodajPracBiura(p);
		
		Pracownik_biura p2 = new Pracownik_biura();
		p2.setLogin("dyrektor");
		try {
			p2.setHaslo(PBKDF2.generateStorngPasswordHash("dyrektor"));
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		} 
		p2.setImie("Amadeusz");
		p2.setNazwisko("Szef");
		p2.setTyp_konta(rodzaj_konta.dyrektor);
		p2.setEmail("amadeusz@szef.pl");
		ps.dodajPracBiura(p2);
		response.sendRedirect("");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
