package com.controllers;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Klient_pracodawca;
import com.service.PBKDF2;
import com.service.UzytkownikPracodawcaService;

/**
 * Servlet implementation class EdycjaDanychPracodawcyServlet
 */
@WebServlet("/EdycjaPracodawcy")
public class EdycjaDanychPracodawcyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EdycjaDanychPracodawcyServlet() {
		super();
	}

	@EJB
	UzytkownikPracodawcaService uzytkownikPracodawca;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");

		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("pracodawca") == 0) {

				RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
				RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/EdycjaDanychPracodawcy.jsp");
				RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

				// Wypelnienie pol aktualnymi danymi
				// ******
				Object value = request.getSession().getAttribute("id");
				Klient_pracodawca p = uzytkownikPracodawca.pobierzDanePracodawcy((Integer) value);
				request.setAttribute("login_atrybut", p.getLogin());
				request.setAttribute("email_atrybut", p.getEmail());
				request.setAttribute("nr_tel_atrybut", p.getNrTel());

				if (p.getNazwa_firmy() == null)
					request.setAttribute("nazwa_firmy_atrybut", "");
				else
					request.setAttribute("nazwa_firmy_atrybut", p.getNazwa_firmy());

				if (p.getAdres() == null)
					request.setAttribute("adres_atrybut", "");
				else
					request.setAttribute("adres_atrybut", p.getAdres());

				if (p.getKraj() == null)
					request.setAttribute("kraj_atrybut", "");
				else
					request.setAttribute("kraj_atrybut", p.getKraj());

				if (p.getRegion() == null)
					request.setAttribute("region_atrybut", "");
				else
					request.setAttribute("region_atrybut", p.getRegion());

				viewNaglowek.include(request, response);
				viewTresc.include(request, response);
				viewStopka.include(request, response);
			} else
				response.sendRedirect("");
		} else
			response.sendRedirect("");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");

		request.setAttribute("bledy", false);

		Object value = request.getSession().getAttribute("id");
		Klient_pracodawca klient = uzytkownikPracodawca.pobierzDanePracodawcy((Integer) value);

		/* --------------------------Walidacja Danych------------------------ */

		/* --------------------------- HASŁO ---------------------------- */
		String haslo = request.getParameter("haslo");
		if (haslo.length() == 0) {
			request.setAttribute("bledy", true);
			request.setAttribute("haslo_blad", true);
		} else {
			try {
				klient.setHaslo(PBKDF2.generateStorngPasswordHash(haslo));
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				request.setAttribute("bledy", true); // ???
				request.setAttribute("haslo_blad", true);
				e.printStackTrace();
			}
		}

		/* --------------------------- E-MAIL --------------------------- */
		String email = request.getParameter("email");
		Pattern wzorAdresuEmail = Pattern.compile(
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		Matcher dopasowanieAdresuEmail = wzorAdresuEmail.matcher(email);
		if (email.length() == 0) {
			request.setAttribute("bledy", true);
			request.setAttribute("email_blad_brak", true);
			request.setAttribute("email_atrybut", "");
		} else if (dopasowanieAdresuEmail.find()) {
			klient.setEmail(email);
			request.setAttribute("email_atrybut", email);
		} else {
			request.setAttribute("bledy", true);
			request.setAttribute("email_blad_wzor", true);
			request.setAttribute("email_atrybut", "");
		}

		/*
		 * ---------------------------- Nr Telefonu
		 * -----------------------------
		 */
		String nrTelefonu = request.getParameter("nr_tel");
		if (nrTelefonu.length() == 9) {
			klient.setNrTel(nrTelefonu);
			request.setAttribute("nr_tel_atrybut", nrTelefonu);
		} else {
			request.setAttribute("bledy", true);
			request.setAttribute("nr_tel_blad", true);
			request.setAttribute("nr_tel_atrybut", "");
		}

		String nazwaFirmy = request.getParameter("nazwa_firmy");
		if (nazwaFirmy.length() != 0)
			klient.setNazwa_firmy(nazwaFirmy);

		String adres = request.getParameter("adres");
		if (adres.length() != 0)
			klient.setAdres(adres);

		String kraj = request.getParameter("kraj");
		if (kraj.length() != 0)
			klient.setKraj(kraj);

		String region = request.getParameter("region");
		if (region.length() != 0)
			klient.setRegion(region);

		if ((Boolean) request.getAttribute("bledy")) {
			doGet(request, response);
		} else {
			synchronized (this) {
				uzytkownikPracodawca.edytujDanePracodawcy(klient);
			}
			response.sendRedirect("");
		}
	}
}
