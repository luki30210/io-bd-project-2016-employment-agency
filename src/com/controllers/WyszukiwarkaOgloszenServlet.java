package com.controllers;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.models.Ogloszenie;
import com.service.OgloszenieService;
/**
 * Servlet implementation class WyszukiwarkaOgloszenServlet
 */
@WebServlet("/WyszukiwarkaOgloszen")
public class WyszukiwarkaOgloszenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	OgloszenieService os;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WyszukiwarkaOgloszenServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		String miasto = request.getParameter("miasto");
		String branza = request.getParameter("branza");
		
		if(branza.length()==0&&miasto.length()==0) response.sendRedirect("");
		
		Integer strona = 1;
		Integer liczbaOgloszenNaStrone = 20;
		
		
		
		Integer liczbaOgloszen = os.getLiczbaOgloszenWyszukanych(miasto, branza);
		
		request.setAttribute("liczba_ogloszen", liczbaOgloszen);
		request.setAttribute("liczba_ogloszen_na_strone", liczbaOgloszenNaStrone);
		request.setAttribute("strona", strona);
		
		List<Ogloszenie> listaOgloszen = (List<Ogloszenie>) os.getOgloszeniaWyszukane(branza, miasto);
		request.setAttribute("lista_ogloszen", listaOgloszen);
		

		
		RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
		RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/StronaGlowna.jsp");
		RequestDispatcher viewOgloszenia = request.getRequestDispatcher("WEB-INF/views/WyswietlOgloszenia.jsp");
		RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

		viewNaglowek.include(request, response);
		viewTresc.include(request, response);
		viewOgloszenia.include(request, response);
		viewStopka.include(request, response);
	}

}
