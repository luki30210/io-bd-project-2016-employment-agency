package com.controllers;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Klient_poszukujacy_pracy;
import com.models.plec;
import com.service.PBKDF2;
import com.service.UzytkownikPoszukujacyPracyService;

/**
 * Servlet implementation class EdycjaDanychPoszukujacegoServlet
 */
@WebServlet("/EdycjaPoszukujacego")
public class EdycjaDanychPoszukujacegoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	UzytkownikPoszukujacyPracyService uzytkownikPoszukujacy;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EdycjaDanychPoszukujacegoServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("poszukujacy") == 0) {
				RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
				RequestDispatcher viewTresc = request
						.getRequestDispatcher("WEB-INF/views/EdycjaDanychPoszukujacego.jsp");
				RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

				Object value = request.getSession().getAttribute("id");
				Klient_poszukujacy_pracy k = uzytkownikPoszukujacy.pobierzDanePoszukujacego((Integer) value);
				request.setAttribute("login_atrybut", k.getLogin());
				request.setAttribute("email_atrybut", k.getEmail());
				request.setAttribute("imie_atrybut", k.getImie());
				request.setAttribute("nazwisko_atrybut", k.getNazwisko());

				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				String formatted = format1.format(k.getDataUrodzenia());
				request.setAttribute("data_urodzenia_atrybut", formatted);

				request.setAttribute("nr_tel_atrybut", k.getNrTel());

				if (k.getPlec() == plec.kobieta)
					request.setAttribute("plec_kobieta", true);
				if (k.getInfoDodatkowe() == null)
					request.setAttribute("inf_dodatkowe_atrybut", "");
				else
					request.setAttribute("inf_dodatkowe_atrybut", k.getInfoDodatkowe());
				if (k.getJezykiObce() == null)
					request.setAttribute("jezyk_atrybut", "");
				else
					request.setAttribute("jezyk_atrybut", k.getJezykiObce());
				if (k.getKategoriaPrawaJazdy() == null)
					request.setAttribute("kategoria_prawa_jazdy_atrybut", "");
				else
					request.setAttribute("kategoria_prawa_jazdy_atrybut", k.getKategoriaPrawaJazdy());

				viewNaglowek.include(request, response);
				viewTresc.include(request, response);
				viewStopka.include(request, response);
			} else
				response.sendRedirect("");
		} else
			response.sendRedirect("");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");

		request.setAttribute("bledy", false);

		Object value = request.getSession().getAttribute("id");
		Klient_poszukujacy_pracy k = uzytkownikPoszukujacy.pobierzDanePoszukujacego((Integer) value);

		/* --------------------------- HASŁO ---------------------------- */
		String haslo = request.getParameter("haslo");
		if (haslo.length() == 0) {
			request.setAttribute("bledy", true);
			request.setAttribute("haslo_blad", true);
		} else {
			try {
				k.setHaslo(PBKDF2.generateStorngPasswordHash(haslo));
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				request.setAttribute("bledy", true);
				request.setAttribute("haslo_blad", true);
				e.printStackTrace();
			}
		}

		/* --------------------------- E-MAIL --------------------------- */
		String email = request.getParameter("email");
		Pattern wzorAdresuEmail = Pattern.compile(
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		Matcher dopasowanieAdresuEmail = wzorAdresuEmail.matcher(email);
		if (email.length() == 0) {
			request.setAttribute("bledy", true);
			request.setAttribute("email_blad_brak", true);
			request.setAttribute("email_atrybut", "");
		} else if (dopasowanieAdresuEmail.find()) {
			k.setEmail(email);
			request.setAttribute("email_atrybut", email);
		} else {
			request.setAttribute("bledy", true);
			request.setAttribute("email_blad_wzor", true);
			request.setAttribute("email_atrybut", "");
		}

		/* -------------------------- NAZWISKO -------------------------- */
		String nazwisko = request.getParameter("nazwisko");
		if (nazwisko.length() == 0) {
			request.setAttribute("bledy", true);
			request.setAttribute("nazwisko_blad", true);
		} else
			k.setNazwisko(nazwisko);

		/* ----------------------- NUMER TELEFONU ----------------------- */
		String nrTelefonu = request.getParameter("nr_tel");
		if (nrTelefonu.length() == 9)
			k.setNrTel(nrTelefonu);
		else {
			request.setAttribute("bledy", true);
			request.setAttribute("nr_tel_blad", true);
		}

		String jezyki = request.getParameter("jezyki");
		if (jezyki.length() != 0)
			k.setJezykiObce(jezyki);

		String kategoria = request.getParameter("kategoria_prawa_jazdy");
		if (kategoria.length() != 0)
			k.setKategoriaPrawaJazdy(kategoria);

		String inf_dod = request.getParameter("inf_dodatkowe");
		if (inf_dod.length() != 0)
			k.setInfoDodatkowe(inf_dod);

		if ((Boolean) request.getAttribute("bledy")) {
			doGet(request, response);
		} else {
			synchronized (this) {
				uzytkownikPoszukujacy.edytujDanePoszukujacego(k);
			}
			response.sendRedirect(""); // Przekierowanie na stronę główną
		}
	}

}
