package com.controllers;

import java.io.IOException;
import java.util.Calendar;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.service.OdwiedzinyService;
import com.service.OgloszenieService;
import com.service.UzytkownikPoszukujacyPracyService;
import com.service.UzytkownikPracodawcaService;

/**
 * Servlet implementation class StatystykiServlet
 */
@WebServlet("/Statystyki")
public class StatystykiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	UzytkownikPoszukujacyPracyService poszukujacy;
	
	@EJB
	UzytkownikPracodawcaService pracodawca;
	
	@EJB
	OgloszenieService ogloszenie;
	
	@EJB
	OdwiedzinyService odwiedziny;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StatystykiServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("obecna_strona", "statystyki");
		response.setContentType("text/html; charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
	    
	    if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("uzytkownik").toString().compareTo("dyrektor") != 0)
				response.sendRedirect("");
		} else
			response.sendRedirect("");
	    
	    request.setAttribute("liczba_poszukujacych", poszukujacy.getLiczbaUzytkownikowPoszukujacych());
	    request.setAttribute("liczba_pracodawcow", pracodawca.getLiczbaUzytkownikowPracodawcow());
	    request.setAttribute("liczba_waznych_ogloszen",ogloszenie.getLiczbaOgloszenZatwierdzonych());
	    request.setAttribute("liczba_niezaakceptowanych_ogloszen",ogloszenie.getLiczbaOgloszenNiezatwierdzonych());
	    
	    Calendar calendar = Calendar.getInstance();
	    Integer dzien = calendar.get(Calendar.DAY_OF_MONTH);
	    Integer miesiac = calendar.get(Calendar.MONTH)+1;
	    Integer rok = calendar.get(Calendar.YEAR);
	    request.setAttribute("liczba_odwiedzin_dzien",odwiedziny.liczbaOdwiedzinDzien(dzien, miesiac, rok));
	    request.setAttribute("liczba_odwiedzin_miesiac",odwiedziny.liczbaOdwiedzinMiesiac(miesiac, rok));
	    request.setAttribute("liczba_odwiedzin_rok",odwiedziny.liczbaOdwiedzinRok(rok));
	    
	    RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
		RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/Statystyki.jsp");
		RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

		viewNaglowek.include(request, response);
		viewTresc.include(request, response);
		viewStopka.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
