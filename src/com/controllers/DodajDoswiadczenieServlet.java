package com.controllers;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Doswiadczenie;
import com.service.DoswiadczenieService;

/**
 * Servlet implementation class DodajDoswiadczenieServlet
 */
@WebServlet("/DodajDoswiadczenie")
public class DodajDoswiadczenieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	DoswiadczenieService ds;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DodajDoswiadczenieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("poszukujacy") != 0)
				response.sendRedirect("");
		} else
			response.sendRedirect("");
		response.setContentType("text/html; charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
	    
	    request.setAttribute("obecna_strona", "dodaj_doswiadczenie");
	    
		RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
		RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/DodajDoswiadczenie.jsp");
		RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

		viewNaglowek.include(request, response);
		viewTresc.include(request, response);
		viewStopka.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("poszukujacy") != 0)
				response.sendRedirect("");
		} else
			response.sendRedirect("");
		
		response.setContentType("text/html; charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		request.setAttribute("obecna_strona", "dodaj_doswiadczenie");
		
		Doswiadczenie doswiadczenie = new Doswiadczenie();
		Object value = request.getSession().getAttribute("id");
		
		doswiadczenie.setStanowisko(request.getParameter("stanowisko"));
		doswiadczenie.setBranza(request.getParameter("branza"));
		
		String rozp = request.getParameter("data_rozp");
		String dataRozpArray[] = rozp.split("-");
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dataRozpArray[2]));
		calendar.set(Calendar.MONTH, Integer.parseInt(dataRozpArray[1]) - 1);
		calendar.set(Calendar.YEAR, Integer.parseInt(dataRozpArray[0]));
		Date date = calendar.getTime();
		doswiadczenie.setData_rozp_pracy(date);
		
		String zak = request.getParameter("data_zak");
		String dataZakArray[] = zak.split("-");
		Calendar calendar2 = Calendar.getInstance();
		calendar2.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dataZakArray[2]));
		calendar2.set(Calendar.MONTH, Integer.parseInt(dataZakArray[1]) - 1);
		calendar2.set(Calendar.YEAR, Integer.parseInt(dataZakArray[0]));
		Date date2 = calendar2.getTime();
		doswiadczenie.setData_zak_pracy(date2);
		
		doswiadczenie.setZdobyte_umiejetnosci(request.getParameter("zdobyte_umiejetnosci"));
		
		ds.dodajDoswiadczenie(doswiadczenie);
		ds.dodajDoswiadczenieDoPoszukujacego(doswiadczenie.getId_doswiadczenia(), (Integer)value);
		response.sendRedirect("");
	}

}
