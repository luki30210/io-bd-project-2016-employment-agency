package com.controllers;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Klient_poszukujacy_pracy;
import com.models.Ogloszenie;
import com.service.OgloszenieService;

/**
 * Servlet implementation class AplikacjeNaOferteServlet
 */
@WebServlet("/AplikacjeNaOferte")
public class AplikacjeNaOferteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	OgloszenieService os;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AplikacjeNaOferteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.sendRedirect("");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		Integer idOgloszenia = Integer.parseInt(request.getParameter("id_ogloszenia"));
		Ogloszenie o = os.getOgloszenie(idOgloszenia);
		
		List<Klient_poszukujacy_pracy> kList = o.getKlient_poszukujacy();
		request.setAttribute("lista_poszukujacych", kList);
		
		RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
		RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/StronaGlowna.jsp");
		RequestDispatcher viewAplikacje = request.getRequestDispatcher("WEB-INF/views/ListaAplikacji.jsp");
		RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

		viewNaglowek.include(request, response);
		viewTresc.include(request, response);
		viewAplikacje.include(request, response);
		viewStopka.include(request, response);
	}

}
