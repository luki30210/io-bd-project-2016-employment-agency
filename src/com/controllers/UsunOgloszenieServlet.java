package com.controllers;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.service.OgloszenieService;

/**
 * Servlet implementation class UsunOgloszenieServlet
 */
@WebServlet("/UsunOgloszenie")
public class UsunOgloszenieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	OgloszenieService os;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsunOgloszenieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer idOgloszenia = Integer.parseInt(request.getParameter("id_ogloszenia"));
		os.usunOgloszenie(idOgloszenia);
		response.sendRedirect("");
	}

}
