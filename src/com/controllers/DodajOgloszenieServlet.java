package com.controllers;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Ogloszenie;
import com.models.poziom_szkoly;
import com.service.OgloszenieService;

/**
 * Servlet implementation class DodajOgloszenie
 */
@WebServlet("/DodajOgloszenie")
public class DodajOgloszenieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	OgloszenieService os;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DodajOgloszenieServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("pracodawca") != 0)
				response.sendRedirect("");
		} else
			response.sendRedirect("");
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setAttribute("obecna_strona", "dodaj_ogloszenie");

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 1);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");

		String formatted = format1.format(cal.getTime());
		request.setAttribute("data_waznosci_atrybut", formatted);

		RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
		RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/DodajOgloszenie.jsp");
		RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

		viewNaglowek.include(request, response);
		viewTresc.include(request, response);
		viewStopka.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("pracodawca") != 0)
				response.sendRedirect("");
		} else
			response.sendRedirect("");
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		request.setAttribute("obecna_strona", "dodaj_ogloszenie");

		request.setAttribute("bledy", false);
		Ogloszenie ogloszenie = new Ogloszenie();
		Object value = request.getSession().getAttribute("id");

		ogloszenie.setBranza(request.getParameter("branza"));
		ogloszenie.setMiasto(request.getParameter("miasto"));

		ogloszenie.setPrzedzialWiekowy(request.getParameter("wiek"));
		ogloszenie.setPoziomWyksztalcenia(poziom_szkoly.valueOf(request.getParameter("poziom_wyksztalcenia")));

		ogloszenie.setDoswiadczenie(request.getParameter("doswiadczenie"));
		ogloszenie.setOpisStanowiska(request.getParameter("opis_stanowiska"));
		ogloszenie.setNazwaFirmy(request.getParameter("nazwa_firmy"));
		ogloszenie.setAdres(request.getParameter("adres"));
		ogloszenie.setKraj(request.getParameter("kraj"));
		ogloszenie.setRegion(request.getParameter("region"));

		String waznosc = request.getParameter("data_waznosci");
		String dataWaznosciArray[] = waznosc.split("-");
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dataWaznosciArray[2]));
		calendar.set(Calendar.MONTH, Integer.parseInt(dataWaznosciArray[1]) - 1);
		calendar.set(Calendar.YEAR, Integer.parseInt(dataWaznosciArray[0]));
		ogloszenie.setTerminWaznosciOgl(calendar.getTime());

		os.dodajOgloszenie(ogloszenie);
		// Aby sprawdzic dzialanie trzeba zarejestrowac sie jako pracodawca i
		// zalogowac sie aby utworzyc sesje
		os.dodajOgloszenieDoPracodawcy(ogloszenie.getId_ogloszenia(), (Integer) value);

		response.sendRedirect("");

	}

}
