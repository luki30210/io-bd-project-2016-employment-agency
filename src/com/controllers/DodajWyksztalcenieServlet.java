package com.controllers;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Wyksztalcenie;
import com.models.poziom_szkoly;
import com.service.WyksztalcenieService;

/**
 * Servlet implementation class DodajWyksztalcenieServlet
 */
@WebServlet("/DodajWyksztalcenie")
public class DodajWyksztalcenieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	WyksztalcenieService os;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DodajWyksztalcenieServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("poszukujacy") != 0)
				response.sendRedirect("");
		} else
			response.sendRedirect("");
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");

		request.setAttribute("obecna_strona", "dodaj_wyksztalcenie");

		request.setAttribute("szkola_atrybut", "");
		request.setAttribute("kierunek_atrybut", "");
		request.setAttribute("specjalizacja_atrybut", "");
		request.setAttribute("data_rozp_atrybut", "");

		RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
		RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/DodajWyksztalcenie.jsp");
		RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

		viewNaglowek.include(request, response);
		viewTresc.include(request, response);
		viewStopka.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("poszukujacy") != 0)
				response.sendRedirect("");
		} else
			response.sendRedirect("");
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");

		request.setAttribute("obecna_strona", "dodaj_wyksztalcenie");

		request.setAttribute("bledy", false);
		Wyksztalcenie wyksztalcenie = new Wyksztalcenie();
		Object value = request.getSession().getAttribute("id");

		String poziom = request.getParameter("poziom_wyksztalcenia");
		wyksztalcenie.setPoziom(poziom_szkoly.valueOf(poziom));
		if (wyksztalcenie.getPoziom() == poziom_szkoly.podstawowa)
			request.setAttribute("podstawowa", true);
		else if (wyksztalcenie.getPoziom() == poziom_szkoly.gimnazjalna)
			request.setAttribute("gimnazjalna", true);
		else if (wyksztalcenie.getPoziom() == poziom_szkoly.srednia)
			request.setAttribute("srednia", true);
		else if (wyksztalcenie.getPoziom() == poziom_szkoly.zawodowa)
			request.setAttribute("zawodowa", true);
		else
			request.setAttribute("wyzsza", true);

		String szkola = request.getParameter("szkola");
		wyksztalcenie.setSzkola(szkola);
		request.setAttribute("szkola_atrybut", szkola);

		String kierunek = request.getParameter("kierunek");
		wyksztalcenie.setKierunek(kierunek);
		request.setAttribute("kierunek_atrybut", kierunek);

		String specjalizacja = request.getParameter("specjalizacja");
		wyksztalcenie.setSpecjalizacja(specjalizacja);
		request.setAttribute("specjalizacja_atrybut", specjalizacja);

		String data_rozp = request.getParameter("data_rozp_nauki");
		request.setAttribute("data_rozp_atrybut", data_rozp);
		String dataRozpArray[] = data_rozp.split("-");
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dataRozpArray[2]));
		calendar.set(Calendar.MONTH, Integer.parseInt(dataRozpArray[1]) - 1);
		calendar.set(Calendar.YEAR, Integer.parseInt(dataRozpArray[0]));
		Date date = calendar.getTime();
		wyksztalcenie.setData_rozp_nauki(date);

		String stan = request.getParameter("stan");
		if (stan.compareTo("tak") == 0)
			wyksztalcenie.setStan(true);
		else {
			wyksztalcenie.setStan(false);
			request.setAttribute("stan_nie", true);
		}

		String data_zak = request.getParameter("data_zak_nauki");
		String wzorDaty = "^(\\d{4}-\\d{1,2}-\\d{1,2})$";
		Pattern r = Pattern.compile(wzorDaty);
		Matcher m = r.matcher(data_zak);
		if (m.find()) {
			String dataZakArray[] = data_zak.split("-");
			Calendar calendar2 = Calendar.getInstance();
			calendar2.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dataZakArray[2]));
			calendar2.set(Calendar.MONTH, Integer.parseInt(dataZakArray[1]) - 1);
			calendar2.set(Calendar.YEAR, Integer.parseInt(dataZakArray[0]));
			wyksztalcenie.setData_zak_nauki(calendar2.getTime());
		} else {
			// System.out.println("Niepoprawna data urodzenia");
			if (stan.compareTo("tak") == 0) {
				request.setAttribute("bledy", true);
				request.setAttribute("data_zak_blad", true);
			}
		}

		if ((Boolean) request.getAttribute("bledy")) {
			RequestDispatcher viewNaglowek = request.getRequestDispatcher("WEB-INF/views/NaglowekStrony.jsp");
			RequestDispatcher viewTresc = request.getRequestDispatcher("WEB-INF/views/DodajWyksztalcenie.jsp");
			RequestDispatcher viewStopka = request.getRequestDispatcher("WEB-INF/views/StopkaStrony.jsp");

			viewNaglowek.include(request, response);
			viewTresc.include(request, response);
			viewStopka.include(request, response);
		} else {
			synchronized (this) {
				os.dodajWyksztalcenie(wyksztalcenie);
				os.dodajWyksztalcenieDoPoszukujacego(wyksztalcenie.getId_wyksztalcenia(),(Integer) value);
			}
			response.sendRedirect("");
		}

	}

}
