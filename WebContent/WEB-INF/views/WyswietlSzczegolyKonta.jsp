<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.models.Klient_poszukujacy_pracy"%>
<%@page import="java.text.Format"%>
<%@page import="java.text.SimpleDateFormat"%>

<div class="kontener">
	<table id="ogloszenia">
		<tr
			style="background-color: rgba(140, 140, 140, 0.5); font-size: 20px; height: 40px;">
			<th style="width: 150px;">Imie</th>
			<th style="width: 150px;">Nazwisko</th>
			<th style="width: 150px;">Data urodzenia</th>
			<th style="width: 70px;">Płeć</th>
			<th style="width: 150px;">Email</th>
			<th style="width: 100px;">Numer telefonu</th>
			<th style="width: 200px;">Kategoria prawa jazdy</th>
			<th style="width: 200px;">Języki obce</th>
			<th style="width: 200px;">Informacje dodatkowe</th>
		</tr>
		<%
			Klient_poszukujacy_pracy k = (Klient_poszukujacy_pracy) request.getAttribute("dane_poszukujacego");
			Format formatter = new SimpleDateFormat("dd-MM-yyyy");
			String data = formatter.format(k.getDataUrodzenia());
		%>
		<tr>
			<td><%=k.getImie()%></td>
			<td><%=k.getNazwisko()%></td>
			<td><%=data%></td>
			<td><%=k.getPlec()%></td>
			<td><%=k.getEmail()%></td>
			<td><%=k.getNrTel()%></td>
			<td><%if(k.getKategoriaPrawaJazdy() == null)%>-<%else {k.getKategoriaPrawaJazdy(); }%></td>
			<td><%if(k.getJezykiObce() == null)%>-<%else {k.getJezykiObce(); }%></td>
			<td><%if(k.getInfoDodatkowe() == null)%>-<%else {k.getInfoDodatkowe(); }%></td>
		</tr>
	</table>
</div>