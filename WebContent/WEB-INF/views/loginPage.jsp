<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="kontener">
	<form action="LoginServlet" method="post">
		<fieldset>
			<legend>Wprowadź dane logowania</legend>
			<div id="poleFormularza">
				<label for="login" class="poleWejscioweEtykieta">Login: </label> <input
					class="poleWejscioweFormularza" name="uzytkownik" type="text"
					placeholder="Nazwa użytkownika" required></input>
			</div>
			<div id="poleFormularza">
				<label for="haslo" class="poleWejscioweEtykieta">Hasło: </label> <input
					class="poleWejscioweFormularza" name="hasloPwd" type="password"
					placeholder="Hasło użytkownika" required></input>
			</div>
		</fieldset>
		<div id="polePrzyciskuPotwierdz">
			<input id="przyciskPotwierdz" type="submit" value="Zaloguj"></input>
		</div>
	</form>
</div>