<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="kontener">
	<!-- 		<div class="tytul">Rejestracja pracodawcy</div> -->

	<%
		if (request.getAttribute("bledy") != null) {
	%>

	<fieldset>
		<legend>Błędy</legend>
		<ul class="lista_bledow">
			<%
				if (request.getAttribute("login_blad_brak") != null) {
			%>
			<li class="blad">Nie podano loginu</li>
			<%
				}
			%>
			<%
				if (request.getAttribute("login_blad_zajety") != null) {
			%>
			<li class="blad">Podany login jest zajęty</li>
			<%
				}
			%>
			<%
				if (request.getAttribute("haslo_blad") != null) {
			%>
			<li class="blad">Nie wprowadzono hasła</li>
			<%
				}
			%>
			<%
				if (request.getAttribute("email_blad_brak") != null) {
			%>
			<li class="blad">Nie podano adresu e-mail</li>
			<%
				}
			%>
			<%
				if (request.getAttribute("email_blad_wzor") != null) {
			%>
			<li class="blad">Wprowadzono niepoprawny adres e-mail</li>
			<%
				}
			%>
			<%
				if (request.getAttribute("nr_tel_blad") != null) {
			%>
			<li class="blad">Nie podano numeru telefonu lub podano błędny</li>
			<%
				}
			%>

		</ul>
	</fieldset>

	<%
		}
	%>

	<form action="Rejestracja2" method="post">
		<fieldset>
			<legend>Wprowadź swoje dane</legend>
			<div id="poleFormularza">
				<label for="login" class="poleWejscioweEtykieta">Login: </label> <input
					class="poleWejscioweFormularza" name="login" type="text"
					placeholder="Nazwa użytkownika"
					value="<%=request.getAttribute("login_atrybut")%>" required></input>
			</div>
			<div id="poleFormularza">
				<label for="haslo" class="poleWejscioweEtykieta">Hasło: </label> <input
					class="poleWejscioweFormularza" name="haslo" type="password"
					placeholder="Hasło użytkownika" required></input>
			</div>
			<div id="poleFormularza">
				<label for="email" class="poleWejscioweEtykieta">E-mail: </label> <input
					class="poleWejscioweFormularza" name="email" type="text"
					placeholder="E-mail użytkownika"
					value="<%=request.getAttribute("email_atrybut")%>" required></input>
			</div>
			<div id="poleFormularza">
				<label for="nr_tel" class="poleWejscioweEtykieta">Nr.
					telefonu: </label> <input class="poleWejscioweFormularza" name="nr_tel"
					type="text" placeholder="Numer telefonu (9 cyfr)"
					value="<%=request.getAttribute("nr_tel_atrybut")%>" required></input>
			</div>
		</fieldset>
		<div id="polePrzyciskuPotwierdz">
			<input id="przyciskPotwierdz" type="submit" value="Potwierdź"></input>
		</div>
	</form>
</div>