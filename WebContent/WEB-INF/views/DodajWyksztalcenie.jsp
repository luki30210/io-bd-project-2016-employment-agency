<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="kontener">
	<%
		if (request.getAttribute("bledy") != null) {
	%>

	<fieldset>
		<legend>Błędy</legend>
		<ul class="lista_bledow">
			<%
				if (request.getAttribute("data_zak_blad") != null) {
			%>
			<li class="blad">Nie podano daty ukończenia szkoły</li>
			<%
				}
			%>
		</ul>
	</fieldset>

	<%
		}
	%>
	<form action="DodajWyksztalcenie" method="post">
		<fieldset>
			<legend>Wprowadź swoje dane dotyczące wykształcenia</legend>
			<div id="poleFormularza">
				<label for="poziom_wyksztalcenia" class="poleWejscioweEtykieta">Poziom
					wykształcenia: </label> <select name="poziom_wyksztalcenia" class="poleWejscioweFormularza"
					id="poleWyboru">
					<option value="podstawowa"
						<%if (request.getAttribute("podstawowa") != null) {%> selected
						<%}%>>Podstawowe</option>
					<option value="gimnazjalna"
						<%if (request.getAttribute("gimnazjalna") != null) {%> selected
						<%}%>>Gimnazjalne</option>
					<option value="srednia"
						<%if (request.getAttribute("srednia") != null) {%> selected <%}%>>Średnia</option>
					<option value="zawodowa"
						<%if (request.getAttribute("zawodowa") != null) {%> selected <%}%>>Zawodowe</option>
					<option value="wyzsza"
						<%if (request.getAttribute("wyzsza") != null) {%> selected <%}%>>Wyższe</option>
				</select>
			</div>
			<div id="poleFormularza">
				<label for="szkola" class="poleWejscioweEtykieta"> Szkoła: </label>
				<input class="poleWejscioweFormularza" name="szkola" type="text"
					placeholder="Szkoła"
					value="<%=request.getAttribute("szkola_atrybut")%>" required></input>
			</div>
			<div id="poleFormularza">
				<label for="kierunek" class="poleWejscioweEtykieta">
					Kierunek: </label> <input class="poleWejscioweFormularza" name="kierunek"
					type="text" placeholder="Kierunek"
					value="<%=request.getAttribute("kierunek_atrybut")%>" required></input>
			</div>
			<div id="poleFormularza">
				<label for="specjalizacja" class="poleWejscioweEtykieta">
					Specjalizacja: </label> <input class="poleWejscioweFormularza"
					name="specjalizacja" type="text" placeholder="Specjalizacja"
					value="<%=request.getAttribute("specjalizacja_atrybut")%>" required></input>
			</div>
			<div id="poleFormularza">
				<label for="data_rozp_nauki" class="poleWejscioweEtykieta">Data
					rozpoczęcia nauki: </label> <input class="poleWejscioweFormularza"
					name="data_rozp_nauki" type="date"
					placeholder="Data rozpoczęcia nauki"
					id="data"
					value="<%=request.getAttribute("data_rozp_atrybut")%>" required></input>
			</div>
			<div id="poleFormularza">
				<label for="data_zak_nauki" class="poleWejscioweEtykieta">Data
					zakończenia nauki: </label> <input class="poleWejscioweFormularza"
					name="data_zak_nauki" type="date"
					id="data2"
					placeholder="Data zakończenia nauki"></input>
			</div>
			<div id="poleFormularza">
				<label for="stan" class="poleWejscioweEtykieta">Czy
					ukończona: </label> <select name="stan" class="poleWejscioweFormularza"
					id="poleWyboru">
					<option value="tak">Tak</option>
					<option value="nie"
						<%if (request.getAttribute("stan_nie") != null) {%> selected <%}%>>Nie</option>
				</select>
			</div>
		</fieldset>
		<div class="poleFormularza" id="polePrzyciskuPotwierdz">
			<input id="przyciskPotwierdz" type="submit" value="Potwierdź"></input>
		</div>
	</form>
</div>
