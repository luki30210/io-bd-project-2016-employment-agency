<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.models.Wyksztalcenie"%>
<%@page import="java.util.List"%>
<%@page import="java.text.Format"%>
<%@page import="java.text.SimpleDateFormat"%>

<div class="kontener">
	<table id="ogloszenia">
		<tr style="background-color: rgba(140, 140, 140, 0.5); font-size: 20px; height: 40px;">
			<th style="width: 140px;">Poziom wykształcenia</th>
			<th style="width: 300px;">Szkoła</th>
			<th style="width: 160px;">Kierunek</th>
			<th style="width: 160px;">Specjalizacja</th>
			<th style="width: 210px;">Data rozpoczęcia nauki</th>
			<th style="width: 210px;">Data zakończenia nauki</th>
		</tr>

		<%
			List<Wyksztalcenie> wList = (List<Wyksztalcenie>) request.getAttribute("lista_wyksztalcenie");
			for (Integer i = 0; i < wList.size(); i++) {
				Format formatter = new SimpleDateFormat("dd-MM-yyyy");
				String data = formatter.format(wList.get(i).getData_rozp_nauki());
				String data2 = formatter.format(wList.get(i).getData_zak_nauki());
		%>
		<tr>
			<td><%=wList.get(i).getPoziom()%></td>
			<td><%=wList.get(i).getSzkola()%></td>
			<td><%=wList.get(i).getKierunek()%></td>
			<td><%=wList.get(i).getSpecjalizacja()%></td>
			<td><%=data%></td>
			<td><%=data2%></td>
		</tr>
		<%
			}
		%>

	</table>
</div>