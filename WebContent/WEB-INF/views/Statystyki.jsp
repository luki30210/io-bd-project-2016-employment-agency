<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="kontener">
	<table id="tabelaSzczegolow">
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Liczba zarejestrowanych
				użytkowników poszukujących pracy:</th>
			<td class="wartoscTabeliSzczegolow"><%=request.getAttribute("liczba_poszukujacych")%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Liczba zarejestrowanych
				użytkowników pracodawców:</th>
			<td class="wartoscTabeliSzczegolow"><%=request.getAttribute("liczba_pracodawcow")%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Liczba ważnych ogłoszeń o
				prace:</th>
			<td class="wartoscTabeliSzczegolow"><%=request.getAttribute("liczba_waznych_ogloszen")%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Liczba ogłoszeń czekających
				na akceptacje:</th>
			<td class="wartoscTabeliSzczegolow"><%=request.getAttribute("liczba_niezaakceptowanych_ogloszen")%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
      		<th class="naglowekTabeliSzczegolow">Liczba odwiedzin dzisiaj:</th>
      		<td class="wartoscTabeliSzczegolow"><%=request.getAttribute("liczba_odwiedzin_dzien")%></td>
    	</tr>
    	<tr class="wierszTabeliSzczegolow">
      		<th class="naglowekTabeliSzczegolow">Liczba odwiedzin w tym miesiącu:</th>
      		<td class="wartoscTabeliSzczegolow"><%=request.getAttribute("liczba_odwiedzin_miesiac")%></td>
    	</tr>
    	<tr class="wierszTabeliSzczegolow">
      		<th class="naglowekTabeliSzczegolow">Liczba odwiedzin w tym roku:</th>
      		<td class="wartoscTabeliSzczegolow"><%=request.getAttribute("liczba_odwiedzin_rok")%></td>
    	</tr>
		
	</table>
</div>