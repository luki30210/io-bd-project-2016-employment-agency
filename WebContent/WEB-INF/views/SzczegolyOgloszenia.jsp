<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="com.models.Ogloszenie"%>
<%@page import="java.text.Format"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>

<%
	Ogloszenie ogloszenie = (Ogloszenie) request.getAttribute("ogloszenie");

	Format formatter = new SimpleDateFormat("dd-MM-yyyy");
	String twoString = formatter.format(ogloszenie.getTerminWaznosciOgl());
%>

<div class="kontener">
	<table id="tabelaSzczegolow">
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Opis stanowiska</th>
			<td class="wartoscTabeliSzczegolow"><%=ogloszenie.getOpisStanowiska()%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Branża</th>
			<td class="wartoscTabeliSzczegolow"><%=ogloszenie.getBranza()%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Miasto</th>
			<td class="wartoscTabeliSzczegolow"><%=ogloszenie.getMiasto()%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Przedział wiekowy</th>
			<td class="wartoscTabeliSzczegolow"><%=ogloszenie.getPrzedzialWiekowy()%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Poziom wykształcenia</th>
			<td class="wartoscTabeliSzczegolow"><%=ogloszenie.getPoziomWyksztalcenia()%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Doświadczenie</th>
			<td class="wartoscTabeliSzczegolow"><%=ogloszenie.getDoswiadczenie()%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Nazwa firmy</th>
			<td class="wartoscTabeliSzczegolow"><%=ogloszenie.getNazwaFirmy()%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Adres</th>
			<td class="wartoscTabeliSzczegolow"><%=ogloszenie.getAdres()%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Kraj</th>
			<td class="wartoscTabeliSzczegolow"><%=ogloszenie.getKraj()%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Region</th>
			<td class="wartoscTabeliSzczegolow"><%=ogloszenie.getRegion()%></td>
		</tr>
		<tr class="wierszTabeliSzczegolow">
			<th class="naglowekTabeliSzczegolow">Termin ważności ogłoszenia</th>
			<td class="wartoscTabeliSzczegolow"><%=twoString%></td>
		</tr>
	</table>
	<%
		if (request.getSession().getAttribute("typ") != null) {
			if (request.getSession().getAttribute("typ").toString().compareTo("poszukujacy") == 0) {
				if(((Boolean) request.getAttribute("zaaplikowane")) != true){
	%>
		<form action="AplikacjaNaOgloszenie" method="post">
			<div id="polePrzyciskuPotwierdz"
				style="text-align: right; margin-right: 50px;">
				<input type="hidden" name="id_ogloszenia" value=<%=ogloszenie.getId_ogloszenia()%> /> 
				<input id="przyciskPotwierdz" type="submit" value="Aplikuj na tę ofertę" style="width: 170px;"></input>
			</div>
		</form>
			<%} else{ %>
				<form>
					<div id="polePrzyciskuPotwierdz" style="text-align: right; margin-right: 50px;">
						<input id="przyciskPotwierdz" type="submit" value="Już zaaplikowane" style="width: 270px; background-color: gray;" disabled="disabled"></input>
					</div>
				</form>
					<%} %>	
	<%
			} else if ((request.getSession().getAttribute("typ").toString().compareTo("pracownik") == 0)
					&& ogloszenie.getPracownik() == null) {
	%>
			<form action="ZatwierdzOgloszenie" method="post">
				<div id="polePrzyciskuPotwierdz" style="text-align: right; margin-right: 50px;">
					<input type="hidden" name="id_ogloszenia" value=<%=ogloszenie.getId_ogloszenia()%> />
					<input id="przyciskPotwierdz" type="submit" value="Zatwierdź ogloszenie" style="width: 190px;"></input>
				</div>
			</form>
			<form action="UsunOgloszenie" method="post">
				<div id="polePrzyciskuPotwierdz" style="text-align: right; margin-right: 50px;">
					<input type="hidden" name="id_ogloszenia" value=<%=ogloszenie.getId_ogloszenia()%> /> 
					<input id="przyciskPotwierdz" type="submit" value="Usuń ogłoszenie"	style="width: 190px;"></input>
				</div>
			</form>
	<%
			} else if ((request.getSession().getAttribute("typ").toString().compareTo("pracodawca") == 0) && (request.getSession().getAttribute("id") == ogloszenie.getKlient().getId_pracodawcy())) {
	%>
			<form action="AplikacjeNaOferte" method="post">
				<div id="polePrzyciskuPotwierdz" style="text-align: right; margin-right: 50px;">
					<input type="hidden" name="id_ogloszenia" value=<%=ogloszenie.getId_ogloszenia()%> /> 
					<input id="przyciskPotwierdz" type="submit" value="Aplikacje na ofertę" style="width: 270px;"></input>
				</div>
			</form>
			<% if (ogloszenie.getTerminWaznosciOgl().after(Calendar.getInstance().getTime())) { %>
				<form action="ArchiwizujOgloszenie" method="post">
					<div id="polePrzyciskuPotwierdz" style="text-align: right; margin-right: 50px;">
						<input type="hidden" name="id_ogloszenia" value=<%=ogloszenie.getId_ogloszenia()%> />
						<input id="przyciskPotwierdz" type="submit" value="Unieważnij ogłoszenie" style="width: 270px;"></input>
					</div>
				</form>
			<% } else { %>
				<form>
					<div id="polePrzyciskuPotwierdz" style="text-align: right; margin-right: 50px;">
						<input id="przyciskPotwierdz" type="submit" value="Ogłoszenie w archiwum" style="width: 270px; background-color: gray;" disabled="disabled"></input>
					</div>
				</form>
			<% } %>
			<!-- Przycisk do edycji ogloszenia -->
			<form action="EdycjaOgloszeniaServlet" method="post"> 
				<div id="polePrzyciskuPotwierdz" style="text-align: right; margin-right: 50px;">
					<input type="hidden" name="id_ogloszenia" value=<%=ogloszenie.getId_ogloszenia()%> />
					<input type="hidden" name="wyswietl" value="true" />
					<input id="przyciskPotwierdz" type="submit" value="Edytuj" style="width: 270px;"></input>
				</div>
			</form>
	<%
			}
		}
	%>
</div>