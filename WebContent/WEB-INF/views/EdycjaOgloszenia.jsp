<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="kontener">
	<form action="EdycjaOgloszeniaServlet" method="post">
		<fieldset>
			<legend>Wprowadź dane</legend>
			<div id="poleFormularza">
				<label for="branza" class="poleWejscioweEtykieta">Branża: </label> <input
					class="poleWejscioweFormularza" name="branza" type="text"
					placeholder="Branża"
					value="<%=request.getAttribute("branza_atrybut")%>" required></input>
			</div>
			<div id="poleFormularza">
				<label for="miasto" class="poleWejscioweEtykieta">Miasto: </label> <input
					class="poleWejscioweFormularza" name="miasto" type="text"
					placeholder="Miasto"
					value="<%=request.getAttribute("miasto_atrybut")%>" required></input>
			</div>
			<div id="poleFormularza">
				<label for="przedzialWiekowy" class="poleWejscioweEtykieta">Przedział
					wiekowy: </label> <input class="poleWejscioweFormularza"
					name="przedzialWiekowy" type="text" placeholder="Przedział Wiekowy"
					value="<%=request.getAttribute("przedzialWiekowy_atrybut")%>"
					required></input>
			</div>
			<div id="poleFormularza">
				<label for="poziomWyksztalcenia" class="poleWejscioweEtykieta">Poziom
					wykształcenia: </label> <select name="poziomWyksztalcenia"
					class="poleWejscioweFormularza" id="poleWyboru" required>
					<option value="podstawowa"
						<%
							if (request.getAttribute("poziomWyksztalcenia_podstawowe") != null) {
						%> selected
						<%
							}
						%>>Podstawowe</option>
					<option value="gimnazjalna"
						<%
							if (request.getAttribute("poziomWyksztalcenia_gimnazjalne") != null) {
						%> selected
						<%
							}
						%>> Gimnazjalne</option>
					<option value="srednia"
						<%
							if(request.getAttribute("poziomWyksztalcenia_zawodowe")!=null) {
						%> selected
						<% 
							}
						%>>Średnie</option>
					<option value="zawodowa"
						<%
							if(request.getAttribute("poziomWyksztalcenia_zawodowe2")!=null) {
						%> selected
						<% 
							}
						%>>Zawodowe</option>
					<option value="wyzsza"
						<%
							if(request.getAttribute("poziomWyksztalcenia_wyzsze")!=null) {
						%> selected
						<% 
							}
						%>>Wyższe</option>
				</select>
				</div>
				<div id="poleFormularza">
					<label for="doswiadczenie" class="poleWejscioweEtykieta">
						Doświadczenie: </label> <input class="poleWejscioweFormularza"
						name="doswiadczenie" type="text" placeholder="Doświadczenie"
						value="<%=request.getAttribute("doswiadczenie_atrybut")%>"
						required></input>
				</div>
				<div id="poleFormularza">
					<label for="opisStanowiska" class="poleWejscioweEtykieta">
						Opis stanowiska: </label> <input class="poleWejscioweFormularza"
						name="opisStanowiska" type="text" placeholder="Opis Stanowiska"
						value="<%=request.getAttribute("opisStanowiska_atrybut")%>"
						required></input>
				</div>
				<div id="poleFormularza">
					<label for="nazwaFirmy" class="poleWejscioweEtykieta">Nazwa
						firmy: </label> <input class="poleWejscioweFormularza" name="nazwaFirmy"
						type="text" placeholder="Nazwa firmy"
						value="<%=request.getAttribute("nazwaFirmy_atrybut")%>" required></input>
				</div>
				<div id="poleFormularza">
					<label for="adres" class="poleWejscioweEtykieta"> Adres: </label> <input
						class="poleWejscioweFormularza" name="adres" type="text"
						placeholder="Adres"
						value="<%=request.getAttribute("adres_atrybut")%>" required></input>
				</div>
				<div id="poleFormularza">
					<label for="kraj" class="poleWejscioweEtykieta"> Kraj: </label> <input
						class="poleWejscioweFormularza" name="kraj" type="text"
						placeholder="Kraj"
						value="<%=request.getAttribute("kraj_atrybut")%>" required></input>
				</div>
				<div id="poleFormularza">
					<label for="region" class="poleWejscioweEtykieta"> Region:
					</label> <input class="poleWejscioweFormularza" name="region" type="text"
						placeholder="Region"
						value="<%=request.getAttribute("region_atrybut")%>" required></input>
				</div>
				<div id="poleFormularza">
					<label for="terminWaznosciOgl" class="poleWejscioweEtykieta">Data
						ważności ogłoszenia: </label> <input name="terminWaznosciOgl" type="date"
						class="poleWejscioweFormularza"
						id="data"
						value="<%=request.getAttribute("terminWaznosciOgl_atrybut")%>" required></input>
				</div>
			</div>
		</fieldset>
		<div id="polePrzyciskuPotwierdz">
			<input type="hidden" name="edytuj" value="true" />
			<input type="hidden" name="id_ogloszenia" value=<%=request.getParameter("id_ogloszenia")%> />
			<input id="przyciskPotwierdz" type="submit" value="Potwierdź"></input>
		</div>
	</form>
</div>
