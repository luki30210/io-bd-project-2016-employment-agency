<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="kontener">
<p style="margin-top: 10px; font-weight: bold; font-size: 18px;">Regulamin serwisu biura pośrednictwa pracy</p>
	<table id="regulamin">
		<tr>
			<td>1.</td>
			<td>Aplikacja dla biura pośrednictwa pracy jest miejscem umieszczania ogłoszeń o poszukiwaniu pracy i o zapotrzebowaniu na pracowników, dalej określana jako aplikacja.</td>
		</tr>
		<tr>
			<td>2.</td>
			<td>Jako użytkownik rozumiemy każdego, kto korzysta z aplikacji.</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Użytkownik umieszczający ogłoszenie oraz wpisując swoje dane do zasobów aplikacji oświadcza, iż zapoznał się z poniższym regulaminem i będzie go przestrzegał.</td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Użytkownik, który zamieszcza swoje dane zgadza się na zbieranie, utrwalanie, przechowywanie, opracowywanie, zmienianie, udostępnianie i opracowywanie udostępnionych danych przez aplikację.</td>
		</tr>
		<tr>
			<td>5.</td>
			<td>Dane gromadzone przez aplikację będą udostępniane tylko i wyłącznie na stronie aplikacji oraz w e-mailach wysyłanych przez aplikację.</td>
		</tr>
		<tr>
			<td>6.</td>
			<td>Informacje zawarte w aplikacji mogą być wykorzystane tylko i wyłącznie do celów zgodnych z prawem. Treść ogłoszeń niezgodnych z prawem oraz wszystkie informacje potrzebne do zlokalizowania ogłoszeniodawcy będą przekazywane odpowiednim organom ścigania.</td>
		</tr>
		<tr>
			<td>7.</td>
			<td>Rezerwujemy prawo do nieakceptacji oraz usuwania ogłoszeń, które:
				<table>
					<tr><td>a)</td> <td>mają wprowadzoną nieodpowiednią branżę,</td></tr>
					<tr><td>b)</td> <td>są w jakikolwiek dla kogokolwiek obraźliwe.</td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>8.</td>
			<td>Rezerwujemy prawo do:
				<table>
					<tr><td>a)</td> <td>usuwania ogłoszeń niezgodnych z prawem lub niniejszym regulaminem</td></tr>
					<tr><td>b)</td> <td>modyfikowania niniejszego regulaminu,</td></tr>
					<tr><td>c)</td> <td>czasowego przerywania pracy serwisu,</td></tr>
					<tr><td>d)</td> <td>zaprzestania świadczeń usług ogłoszeniowych.</td></tr>
				</table>
			</td>
		</tr>
	</table>
</div>

