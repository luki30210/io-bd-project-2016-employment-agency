<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.models.Doswiadczenie"%>
<%@page import="java.util.List"%>
<%@page import="java.text.Format"%>
<%@page import="java.text.SimpleDateFormat"%>


<div class="kontener">
	<table id="ogloszenia">
		<tr
			style="background-color: rgba(140, 140, 140, 0.5); font-size: 20px; height: 40px;">
			<th style="width: 150px;">Stanowisko</th>
			<th style="width: 150px;">Branża</th>
			<th style="width: 200px;">Data rozpoczęcia pracy</th>
			<th style="width: 200px;">Data zakończenia pracy</th>
			<th style="width: 500px;">Zdobyte umiejętności</th>
		</tr>

		<%
			List<Doswiadczenie> dList = (List<Doswiadczenie>) request.getAttribute("lista_doswiadczen");

			for (Integer i = 0; i < dList.size(); i++) {
				
				Format formatter = new SimpleDateFormat("dd-MM-yyyy");
				String data = formatter.format(dList.get(i).getData_rozp_pracy());
				String data2 = formatter.format(dList.get(i).getData_zak_pracy());
		%>
		<tr>
			<td><%=dList.get(i).getStanowisko()%></td>
			<td><%=dList.get(i).getBranza()%></td>
			<td><%=data%></td>
			<td><%=data2%></td>
			<td><%=dList.get(i).getZdobyte_umiejetnosci()%></td>
		</tr>
		<%
			}
		%>
	</table>
</div>