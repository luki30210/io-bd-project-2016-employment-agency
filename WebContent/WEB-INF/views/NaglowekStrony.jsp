<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Biuro Pośrednictwa Pracy</title>
<link rel="stylesheet" href="resources/css/normalizacja.css" />
<link rel="stylesheet" href="resources/css/motyw.css" />
<link rel="icon" href="resources/ikona.png">
<link href='https://fonts.googleapis.com/css?family=Lato:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
 	<script type="text/javascript" src="resources/js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" >
   var main = function() {
  	  $('.uzytkownik').mouseover(function() {
  	    $('.menu').show();
  	  });

  	$('.pasek').mouseleave(function() {
  	    $('.menu').hide();
  	  });
  	};


  	$(document).ready(main);
    </script>
    <script type="text/javascript">
    var datefield=document.createElement("input")
    datefield.setAttribute("type", "date")
    if (datefield.type!="date"){
        document.write('<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />\n')
        document.write('<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"><\/script>\n')
        document.write('<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"><\/script>\n')
	}
	</script>
	<script>
	if (datefield.type!="date"){
        jQuery(function($){
            $('#data').datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $('#data2').datepicker({
                dateFormat: 'yy-mm-dd'
            });
        })
    }
	</script>
</head>
<body>
 
	<div id="wrapper">
		<div id="naglowek">
			<div id="logo" style="font-size: 45px">
				<img alt="Biuro Pośrednictwa Pracy" src="resources/logo.png">
			</div>
			<div class="pasek">
 				<div class="menu">	<!-- dobrze będzie wyglądało dla trzech linijek, dla czterech trzeba zmienić CSS = trzymajmy się trzech xD -->
 						<% if(request.getSession().getAttribute("typ") == "poszukujacy") { %>
 						<ul>
 							<li><a href="WyswietlSzczegoly">Profil</a><br></li>
							<li><a href="EdycjaPoszukujacego">Edycja danych</a><br></li>
							<li><a href="UsunKonto">Usuń konto</a><br></li>
						</ul>
						<% } else if (request.getSession().getAttribute("typ") == "pracodawca") { %>
						<ul>
							<li><a href="EdycjaPracodawcy">Edycja danych</a><br></li>
							<li><a href="UsunKonto">Usuń konto</a><br></li>
						</ul>
						<% } %>
						
				</div>
				<div class="uzytkownik">
					<img 
					src="/BiuroPosrednictwaPracyBCC/resources/userlogo.png"
					alt="Użytkownik" style="width: 64px; height: 64px;">
				</div>
			</div>
		</div>
		<div>
			<ol class="pasekNawigacji">
				<li><a <% if (request.getAttribute("obecna_strona") == "strona_glowna") { %> class="active" <% } %> href="/BiuroPosrednictwaPracyBCC">Home</a></li>
				<li><a <% if (request.getAttribute("obecna_strona") == "regulamin") { %> class="active" <% } %> href="Regulamin">Regulamin</a></li>
				<%	if (request.getSession().getAttribute("typ") == null) { %>
					<li><a <% if (request.getAttribute("obecna_strona") == "logowanie") { %> class="active" <% } %> href="LoginServlet">Logowanie</a></li>
					<li><a <% if (request.getAttribute("obecna_strona") == "rejestracja1") { %> class="active" <% } %> href="Rejestracja1" style="padding: 4px 6px;">Rejestracja jako poszukujący</a></li>
					<li><a <% if (request.getAttribute("obecna_strona") == "rejestracja2") { %> class="active" <% } %> href="Rejestracja2" style="padding: 4px 6px;">Rejestracja jako pracodawca</a></li>
				<% 	} else { %>				
				<%		if(request.getSession().getAttribute("typ") == "poszukujacy") { %>
						<li><a <% if (request.getAttribute("obecna_strona") == "dodaj_doswiadczenie") { %> class="active" <% } %> href="DodajDoswiadczenie">Dodaj doświadczenie</a></li>
						<li><a <% if (request.getAttribute("obecna_strona") == "dodaj_wyksztalcenie") { %> class="active" <% } %> href="DodajWyksztalcenie">Dodaj wykształcenie</a></li>
				<%
						} else if (request.getSession().getAttribute("typ") == "pracodawca") {
				%>
						<li><a <% if (request.getAttribute("obecna_strona") == "dodaj_ogloszenie") { %> class="active" <% } %> href="DodajOgloszenie">Dodaj ogłoszenie</a></li>
						<li><a <% if (request.getAttribute("obecna_strona") == "moje_ogloszenia") { %> class="active" <% } %> href="MojeOgloszenia">Moje ogłoszenia</a></li>
				<%
						} else if (request.getSession().getAttribute("typ") == "pracownik") {
				%>
						<%if (request.getSession().getAttribute("administrator") != null) { %>
						<li><a <% if (request.getAttribute("obecna_strona") == "panel_pracownika") { %> class="active" <% } %> href="PanelPracownika">Panel administratora</a></li>
						<%} else { %>
						<li><a <% if (request.getAttribute("obecna_strona") == "statystyki") { %> class="active" <% } %> href="Statystyki">Statystyki</a></li>
				<%
						}
						}
				%>
					<li><a href="LogoutServlet">Wyloguj się</a></li>
				<%
					}
				%>
			</ol>
		</div>
