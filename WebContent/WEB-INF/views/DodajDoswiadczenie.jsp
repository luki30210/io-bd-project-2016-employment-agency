<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="kontener">
	<form action="DodajDoswiadczenie" method="post">
		<fieldset>
			<legend>Wprowadź dane do doświadczenia</legend>
			<div id="poleFormularza">
				<label for="stanowisko" class="poleWejscioweEtykieta">Stanowisko:
				</label> <input class="poleWejscioweFormularza" name="stanowisko"
					type="text" placeholder="Stanowisko" required></input>
			</div>
			<div id="poleFormularza">
				<label for="branza" class="poleWejscioweEtykieta">Branża: </label> <input
					class="poleWejscioweFormularza" name="branza" type="text"
					placeholder="Branza" required></input>
			</div>
			<div id="poleFormularza">
				<label for="data_rozp" class="poleWejscioweEtykieta">Data
					rozpoczęcia: </label> <input class="poleWejscioweFormularza"
					name="data_rozp" type="date" placeholder="Data rozpoczęcia pracy" id="data"
					required></input>
			</div>
			<div id="poleFormularza">
				<label for="data_zak" class="poleWejscioweEtykieta">Data
					zakończenia: </label> <input class="poleWejscioweFormularza"
					name="data_zak" type="date" placeholder="Data zakończenia pracy" id="data2"
					required></input>
			</div>
			<div id="poleFormularza">
				<label for="zdobyte_umiejetnosci" class="poleWejscioweEtykieta">Zdobyte
					umiejętności: </label> <input class="poleWejscioweFormularza"
					name="zdobyte_umiejetnosci" type="text"
					placeholder="Zdobyte umiejętności"></input>
			</div>
		</fieldset>
		<div id="polePrzyciskuPotwierdz">
			<input id="przyciskPotwierdz" type="submit" value="Potwierdź"></input>
		</div>
	</form>
</div>