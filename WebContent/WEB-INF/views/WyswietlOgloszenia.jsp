<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="com.models.Ogloszenie"%>
<%@page import="java.util.List"%>

<div class="kontener">
	<table id="ogloszenia">
		<tr style="background-color: rgba(140, 140, 140, 0.5); font-size: 18px; height: 40px;">
			<th style="width: 500px;">Opis stanowiska</th>
			<th style="width: 250px;">Branża</th>
			<th style="width: 150px;">Miasto</th>
			<!-- <th>Wiek</th>
			<th>Poziom wykształcenia</th>
			<th>Doświadczenie</th> -->
			<th style="width: 300px;">Nazwa firmy</th>
			<!-- <th>Adres</th>
			<th>Kraj</th>
			<th>Region</th>
			<th>Termin ważności ogłoszenia</th> -->
		</tr>

		<%
			request.setCharacterEncoding("UTF-8");
			Integer liczbaOgloszenNaStrone = (Integer)request.getAttribute("liczba_ogloszen_na_strone");
			Integer liczbaOgloszen = (Integer)request.getAttribute("liczba_ogloszen");
			Integer strona = (Integer)request.getAttribute("strona");
			Integer liczbaStron = (liczbaOgloszen+liczbaOgloszenNaStrone-1)/liczbaOgloszenNaStrone;
			List<Ogloszenie> oList = (List<Ogloszenie>) request.getAttribute("lista_ogloszen");

			for (Integer i = 0; i < oList.size(); i++) {
		%>
		<tr style="<% if ((i%2)==1) { %> background-color: rgba(140, 140, 140, 0.2); <% }%> height: 35px;">
			<td style="text-decoration: underline; color: rgba(80, 80, 255, 1);">
				<form action="SzczegolyOgloszenia" method="post">
					<input type="hidden" name="id_ogloszenia" value=<%=oList.get(i).getId_ogloszenia() %> />
					<input type="submit" value="<%=oList.get(i).getOpisStanowiska()%>" id="przyciskJakLink">
					
				</form>
			</td>
			<td><%=oList.get(i).getBranza()%></td>
			<td><%=oList.get(i).getMiasto()%></td>
			<%-- <td><%=oList.get(i).getPrzedzialWiekowy()%></td>
			<td><%=oList.get(i).getPoziomWyksztalcenia()%></td>
			<td><%=oList.get(i).getDoswiadczenie()%></td> --%>
			<td><%=oList.get(i).getNazwaFirmy()%></td>
			<%-- <td><%=oList.get(i).getAdres()%></td>
			<td><%=oList.get(i).getKraj()%></td>
			<td><%=oList.get(i).getRegion()%></td>
			<td><%=oList.get(i).getTerminWaznosciOgl()%></td> --%>
		</tr>
		<%
			}
		%>
	</table>
	<div id="stronyOgloszen">
		Strona <%=strona%> z <%=liczbaStron%> (wszystkich ogłoszeń: <%=liczbaOgloszen%>)
		<br>
			<form action="" method="post" style="margin: auto; display: inline;">
				<input type="hidden" name="strona" value=<%=strona-1 %> />
				<input type="submit" value="<%="<<" %>" <%if(strona<=1) { %> disabled="disabled" <% } %>>
			</form>
			<form action="" method="post" style="margin: auto; display: inline;">
				<input type="hidden" name="strona" value=<%=strona+1 %> />
				<input type="submit" value=">>"  <%if(strona>=liczbaStron) { %> disabled="disabled" <% } %>>
			</form>
	</div>
</div>
