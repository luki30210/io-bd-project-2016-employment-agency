<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="kontener">
	<form action="DodajOgloszenie" method="post">
		<fieldset>
			<legend>Wprowadź dane do ogłoszenia</legend>
			<div id="poleFormularza">
				<label for="branza" class="poleWejscioweEtykieta">Branża: </label> <input class="poleWejscioweFormularza"
					name="branza" type="text" placeholder="Branża" required></input>
			</div>
			<div id="poleFormularza">
				<label for="miasto" class="poleWejscioweEtykieta">Miasto: </label> <input class="poleWejscioweFormularza"
					name="miasto" type="text" placeholder="Miasto" required></input>
			</div>
			<div id="poleFormularza">
				<label for="wiek" class="poleWejscioweEtykieta">Wiek: </label> <input class="poleWejscioweFormularza"
					name="wiek" type="text" placeholder="Wiek(od-do)" required></input>
			</div>
			<div id="poleFormularza">
				<label for="poziom_wyksztalcenia" class="poleWejscioweEtykieta">Poziom
					wykształcenia </label> <select name="poziom_wyksztalcenia" class="poleWejscioweFormularza" id="poleWyboru">
					<option value="podstawowa">Podstawowe</option>
					<option value="gimnazjalna">Gimnazjalne</option>
					<option value="zawodowa">Średnie</option>
					<option value="zawodowa">Zawodowe</option>
					<option value="wyzsza">Wyższe</option>
				</select>
			</div>
			<div id="poleFormularza">
				<label for="doswiadczenie" class="poleWejscioweEtykieta">Doświadczenie:
				</label> <input class="poleWejscioweFormularza" name="doswiadczenie" type="text" placeholder="Doświadczenie"
					required></input>
			</div>
			<div id="poleFormularza">
				<label for="opis_stanowiska" class="poleWejscioweEtykieta">Opis
					stanowiska: </label> <input class="poleWejscioweFormularza" name="opis_stanowiska" type="text"
					placeholder="Opis stanowiska" required></input>
			</div>
			<div id="poleFormularza">
				<label for="nazwa_firmy" class="poleWejscioweEtykieta">Nazwa
					firmy: </label> <input class="poleWejscioweFormularza" name="nazwa_firmy" type="text"
					placeholder="Nazwa firmy" required></input>
			</div>
			<div id="poleFormularza">
				<label for="adres" class="poleWejscioweEtykieta">Adres: </label> <input class="poleWejscioweFormularza" 
					name="adres" type="text" placeholder="Adres" required></input>
			</div>
			<div id="poleFormularza">
				<label for="kraj" class="poleWejscioweEtykieta">Kraj: </label> <input
					class="poleWejscioweFormularza" name="kraj" type="text" placeholder="Kraj" required></input>
			</div>
			<div id="poleFormularza">
				<label for="region" class="poleWejscioweEtykieta">Region: </label> <input
					class="poleWejscioweFormularza" name="region" type="text" placeholder="Region" required></input>
			</div>
			<div id="poleFormularza">
				<label for="data_waznosci" class="poleWejscioweEtykieta">Data
					ważności ogłoszenia: </label> <input name="data_waznosci" type="date" class="poleWejscioweFormularza" id="data"
					value="<%=request.getAttribute("data_waznosci_atrybut")%>"></input>
			</div>
		</fieldset>
		<div id="polePrzyciskuPotwierdz">
			<input id="przyciskPotwierdz" type="submit" value="Potwierdź"></input>
		</div>
	</form>
</div>