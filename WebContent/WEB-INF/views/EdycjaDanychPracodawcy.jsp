<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="kontener">
	<!-- 		<div class="tytul">Rejestracja pracodawcy</div> -->

	<%
		if (request.getAttribute("bledy") != null) {
	%>

	<fieldset>
		<legend>Błędy</legend>
		<ul class="lista_bledow">
			<%
				if (request.getAttribute("haslo_blad") != null) {
			%>
			<li class="blad">Nie wprowadzono hasła</li>
			<%
				}
			%>
			<%
				if (request.getAttribute("email_blad_brak") != null) {
			%>
			<li class="blad">Nie podano adresu e-mail</li>
			<%
				}
			%>
			<%
				if (request.getAttribute("email_blad_wzor") != null) {
			%>
			<li class="blad">Wprowadzono niepoprawny adres e-mail</li>
			<%
				}
			%>
			<%
				if (request.getAttribute("nr_tel_blad") != null) {
			%>
			<li class="blad">Nie podano numeru telefonu lub podano błędny</li>
			<%
				}
			%>

		</ul>
	</fieldset>

	<%
		}
	%>

	<form action="EdycjaPracodawcy" method="post">
		<fieldset>
			<legend>Wprowadź swoje dane</legend>
			<div id="poleFormularza">
				<label for="login" class="poleWejscioweEtykieta">Login: </label> <input
					class="poleWejscioweFormularza" name="login" type="text"
					placeholder="Nazwa użytkownika"
					value="<%=request.getAttribute("login_atrybut")%>" 
					disabled="disabled"></input>
			</div>
			<div id="poleFormularza">
				<label for="haslo" class="poleWejscioweEtykieta">Hasło: </label> <input
					class="poleWejscioweFormularza" name="haslo" type="password"
					placeholder="Hasło użytkownika" required></input>
			</div>
			<div id="poleFormularza">
				<label for="email" class="poleWejscioweEtykieta">E-mail: </label> <input
					class="poleWejscioweFormularza" name="email" type="text"
					placeholder="E-mail użytkownika"
					value="<%=request.getAttribute("email_atrybut")%>" required></input>
			</div>
			<div id="poleFormularza">
				<label for="nr_tel" class="poleWejscioweEtykieta">Nr.
					telefonu: </label> <input class="poleWejscioweFormularza" name="nr_tel"
					type="text" placeholder="Numer telefonu (9 cyfr)"
					value="<%=request.getAttribute("nr_tel_atrybut")%>" required></input>
			</div>
			<div id="poleFormularza">
				<label for="nazwa_firmy" class="poleWejscioweEtykieta">Nazwa 
				firmy: </label> <input class="poleWejscioweFormularza" name="nazwa_firmy"
				type="text" placeholder="Nazwa firmy"
				value="<%=request.getAttribute("nazwa_firmy_atrybut")%>"></input>
			</div>
			<div id="poleFormularza">
				<label for="adres" class="poleWejscioweEtykieta"> Adres: 
				</label> <input class="poleWejscioweFormularza" name="adres"
				type="text" placeholder="Adres"
				value="<%=request.getAttribute("adres_atrybut")%>"></input>
			</div>
			<div id="poleFormularza">
				<label for="kraj" class="poleWejscioweEtykieta"> Kraj: 
				</label> <input class="poleWejscioweFormularza" name="kraj"
				type="text" placeholder="kraj"
				value="<%=request.getAttribute("kraj_atrybut")%>"></input>
			</div>
			<div id="poleFormularza">
				<label for="region" class="poleWejscioweEtykieta"> Region: 
				</label> <input class="poleWejscioweFormularza" name="region"
				type="text" placeholder="region"
				value="<%=request.getAttribute("region_atrybut") %>"></input>
			</div>
		</fieldset>
		<div id="polePrzyciskuPotwierdz">
			<input id="przyciskPotwierdz" type="submit" value="Potwierdź"></input>
		</div>
	</form>
</div>