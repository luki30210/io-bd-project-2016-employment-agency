<%@page import="com.models.Klient_poszukujacy_pracy"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="com.models.Klient_pracodawca"%>
<%@page import="java.util.List"%>

<div class="kontener">
	<table id="ogloszenia">
		<tr style="background-color: rgba(140, 140, 140, 0.5); font-size: 20px; height: 40px;">
			<th style="width: 350px;">Imie</th>
			<th style="width: 350px;">Nazwisko</th>
			<th style="width: 300px;">Data urodzenia</th>
			<th style="width: 200px;">Płeć</th>
		</tr>
		<%
			List<Klient_poszukujacy_pracy> kList = (List<Klient_poszukujacy_pracy>) request.getAttribute("lista_poszukujacych");

			for (Integer i = 0; i < kList.size(); i++) {
		%>
		<tr style="<% if ((i%2)==1) { %> background-color: rgba(140, 140, 140, 0.2); <% }%> height: 35px;">
			<td style="text-decoration: underline; color: rgba(80, 80, 255, 1);">
				<form action="WyswietlSzczegoly" method="post">
					<input type="hidden" name="id_klienta" value=<%=kList.get(i).getIdKlienta() %> />
					<input type="submit" value="<%=kList.get(i).getImie()%>" id="przyciskJakLink">
					
				</form>
			</td>
			<td><%=kList.get(i).getNazwisko()%></td>
			<td><%=kList.get(i).getDataUrodzenia()%></td>
			<td><%=kList.get(i).getPlec()%></td>
		</tr>
		<%
			}
		%>
	</table>
</div>
