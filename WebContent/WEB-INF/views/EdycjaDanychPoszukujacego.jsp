<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="kontener">
	<!-- 		<div class="tytul">Edycja danych użytkownika poszukującego pracy</div> -->

	<%
		if (request.getAttribute("bledy") != null) {
	%>

	<fieldset>
		<legend>Błędy</legend>
		<ul class="lista_bledow">
			<%
				if (request.getAttribute("haslo_blad") != null) {
			%>
			<li class="blad">Nie wprowadzono hasła</li>
			<%
				}
			%>
			<%
				if (request.getAttribute("email_blad_brak") != null) {
			%>
			<li class="blad">Nie podano adresu e-mail</li>
			<%
				}
			%>
			<%
				if (request.getAttribute("email_blad_wzor") != null) {
			%>
			<li class="blad">Wprowadzono niepoprawny adres e-mail</li>
			<%
				}
			%>
			<%
				if (request.getAttribute("nazwisko_blad") != null) {
			%>
			<li class="blad">Nie podano nazwiska</li>
			<%
				}
			%>
			<%
				if (request.getAttribute("nr_tel_blad") != null) {
			%>
			<li class="blad">Nie podano numeru telefonu lub podano błędny</li>
			<%
				}
			%>
		</ul>
	</fieldset>

	<%
		}
	%>

	<form action="EdycjaPoszukujacego" method="post">
		<fieldset>
			<legend>Wprowadź swoje dane</legend>
			<div id="poleFormularza">
				<label for="login" class="poleWejscioweEtykieta">Login: </label> <input
					class="poleWejscioweFormularza" name="login" type="text"
					placeholder="Nazwa użytkownika"
					value="<%=request.getAttribute("login_atrybut")%>"
					disabled="disabled"></input>
			</div>
			<div id="poleFormularza">
				<label for="haslo" class="poleWejscioweEtykieta">Hasło: </label> <input
					class="poleWejscioweFormularza" name="haslo" type="password"
					placeholder="Hasło użytkownika" required></input>
			</div>
			<div id="poleFormularza">
				<label for="email" class="poleWejscioweEtykieta">E-mail: </label> <input
					class="poleWejscioweFormularza" name="email" type="text"
					placeholder="E-mail użytkownika"
					value="<%=request.getAttribute("email_atrybut")%>" required></input>
			</div>
			<div id="poleFormularza">
				<label for="imie" class="poleWejscioweEtykieta">Imię: </label> <input
					class="poleWejscioweFormularza" name="imie" type="text"
					placeholder="Twoje imię/imiona"
					value="<%=request.getAttribute("imie_atrybut")%>"
					disabled="disabled"></input>
			</div>
			<div id="poleFormularza">
				<label for="nazwisko" class="poleWejscioweEtykieta">Nazwisko:
				</label> <input name="nazwisko" type="text" placeholder="Twoje nazwisko"
					class="poleWejscioweFormularza"
					value="<%=request.getAttribute("nazwisko_atrybut")%>" required></input>
			</div>
			<div id="poleFormularza">
				<label for="nr_tel" class="poleWejscioweEtykieta">Nr.
					telefonu: </label> <input name="nr_tel" type="text"
					class="poleWejscioweFormularza"
					placeholder="Numer telefonu (9 cyfr)"
					value="<%=request.getAttribute("nr_tel_atrybut")%>" required></input>
			</div>
			<div id="poleFormularza">
				<label for="data_urodzenia" class="poleWejscioweEtykieta">Data
					urodzenia: </label> <input name="data_urodzenia" type="date"
					class="poleWejscioweFormularza"
					id="data"
					value="<%=request.getAttribute("data_urodzenia_atrybut")%>"
					disabled="disabled"></input>
			</div>
			<div id="poleFormularza">
				<label for="plec" class="poleWejscioweEtykieta">Płeć: </label> <select
					name="plec" class="poleWejscioweFormularza" id="poleWyboru"
					disabled="disabled">
					<option value="mezczyzna">Mężczyzna</option>
					<option value="kobieta"
						<%if (request.getAttribute("plec_kobieta") != null) {%> selected
						<%}%>>Kobieta</option>
				</select>
			</div>
			<div id="poleFormularza">
				<label for="jezyki" class="poleWejscioweEtykieta">Języki: </label> <input
					name="jezyki" type="text" class="poleWejscioweFormularza"
					placeholder="Język(stopień)"
					value="<%=request.getAttribute("jezyk_atrybut")%>"></input>
			</div>
			<div id="poleFormularza">
				<label for="kategoria_prawa_jazdy" class="poleWejscioweEtykieta">Kategoria
					prawa jazdy: </label> <input name="kategoria_prawa_jazdy" type="text"
					class="poleWejscioweFormularza"
					placeholder="Kategoria prawa jazdy:"
					value="<%=request.getAttribute("kategoria_prawa_jazdy_atrybut")%>"></input>
			</div>
			<div id="poleFormularza">
				<label for="inf_dodatkowe" class="poleWejscioweEtykieta">Informacje
					dodatkowe: </label>
				<textarea maxlength="200" name="inf_dodatkowe"
					class="poleWejscioweFormularza"
					style="height: 100px; resize: none;"><%=request.getAttribute("inf_dodatkowe_atrybut")%></textarea>
			</div>
		</fieldset>
		<div id="polePrzyciskuPotwierdz">
			<input id="przyciskPotwierdz" type="submit" value="Potwierdź"></input>
		</div>
	</form>
</div>